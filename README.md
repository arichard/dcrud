# dcrud

## Build and run a docker container

### Build

Requires docker. Skips postgres integration tests.

```console
$ docker build -t dcrud .
```

### Run via docker

```console
$ docker run -it --rm -e PGHOST=<postgres hostname> dcrud
```

Where `<postgres hostname>` is the hostname of the postgres server

### Run via kubernetes

```console
$ sed 's/${DCRUD_PGHOST}/<postgres hostname>/g; s/${DCRUD_TAG}/latest/g' < k8s_example.yaml | kubectl apply -f -
```

## Build and run an executable

Result is placed in `bin/dcrud`.

### Reduced dependency build

Requires a POSIX compatible shell and docker.

```console
$ ./docker_build.sh
```

### Native build

Requires a POSIX compatible shell, go, and a locally running postgres server.

```
$ ./build.sh
```

### Run

```
bin/dcrud -c config.yaml
```

Where `config.yaml` is something like

```yaml
postgres:
  hosts:
  - <postgres hostname>
```

See [config.go](internal/dconfig/config.go) for details
