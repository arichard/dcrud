#!/bin/sh
set -e

cd -- "$(dirname "$(realpath "$0")")"

VCS="$(git -c log.showsignature=false show --format=%ct:%H --no-patch)"
VCS_TIME="$(echo "$VCS" | cut -d : -f 1)"
VCS_REVISION="$(echo "$VCS" | cut -d : -f 2)"

VERSION_PATCH="$(git log --format=format:%H | wc -l)"
for REMOTE in $(git remote); do
    URL="$(git remote get-url "$REMOTE")"
    if [ "$URL" = "https://codeberg.org/arichard/dcrud.git" ] || [ "$URL" = "git@codeberg.org:arichard/dcrud.git" ]; then
	REV_RIGHT="$(git rev-list --right-only --count "$REMOTE"/main...HEAD)"
	if [ "$REV_RIGHT" != 0 ]; then
	    VERSION_PATCH="$(echo "$VERSION_PATCH - $REV_RIGHT + 1" | bc)"
	    BRANCH="-$(git rev-parse --abbrev-ref HEAD)-$REV_RIGHT"
	fi
    fi
done

if [ -z "$(git status --porcelain)" ]; then
    CREATED="$VCS_TIME"
else
    NOW="$(date --utc +%s)"
    DIRTY="-dirty-$(printf "%08d\n" "$(echo "$NOW - $VCS_TIME" | bc)")"
    CREATED="$NOW"
fi

VERSION="0.1.$VERSION_PATCH$BRANCH$DIRTY"
# Following https://github.com/opencontainers/image-spec/blob/main/annotations.md label conventions
docker build \
    --label org.opencontainers.image.created="$(date --date=@"$CREATED" --utc --rfc-3339=seconds)" \
    --label org.opencontainers.image.revision="$VCS_REVISION" \
    --label org.opencontainers.image.version="$VERSION" \
    --tag dcrud:"$VERSION" \
    .
