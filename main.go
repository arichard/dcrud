package main

import (
	"context"
	"errors"
	"log"
	"os"
	"os/signal"
	"syscall"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dlog"
	"codeberg.org/arichard/dcrud/internal/foo"
)

func main() {
	var logger dlog.Logger = log.New(os.Stderr, "", log.Ldate|log.Ltime|log.Lmicroseconds)
	ctx, cancel := context.WithCancel(context.Background())
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		sig := <-sigs
		logger.Printf("Caught signal %v, canceling context", sig)
		cancel()
		<-sigs
		logger.Printf("Caught signal %v, exiting", sig)
		os.Exit(1)
	}()
	if err := foo.Bar(ctx, logger, os.Args); err != nil {
		var ece derrors.ExitCodeError
		if errors.As(err, &ece) {
			os.Exit(ece.ExitCode())
		}
		derrors.LogErr(logger, err)
		os.Exit(1)
	}
	os.Exit(0)
}
