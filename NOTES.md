Move data from one column to another without any downtime with concurrent read/writes

1. App v1: read: foo.orig, write: foo.orig
  - possible states:
    - orig: v
  - possible state changes:
    - orig: v -> v1 reads v
    - orig: v -> v1 writes v' -> orig: v'
2. Deploy v2: read: foo.orig, write: foo.orig & foo.new
  a. predeploy: `ALTER TABLE foo ADD COLUMN new type`
    - possible states:
      - orig: v new: null
    - possible state changes:
      - orig: v new: null -> v1 reads v
      - orig: v new: null -> v1 writes v' -> orig: v' new: null
  b. Start deploying v2
    - possible states:
      - orig: v new: null
      - orig: v new: v
      - orig: v' new: v
    - possible state changes:
      - orig: v new: null -> v1 reads v
      - orig: v new: null -> v1 writes v' -> orig: v' new: null
      - orig: v new: null -> v2 reads v
      - orig: v new: null -> v2 writes v' -> orig: v' new: v'
      - orig: v new: v -> v1 reads v
      - orig: v new: v -> v1 writes v' -> orig: v' new: v
      - orig: v new: v -> v2 reads v
      - orig: v new: v -> v2 writes v' -> orig: v' new: v'
      - orig: v' new: v -> v1 reads v'
      - orig: v' new: v -> v1 writes v'' -> orig: v'' new: v
      - orig: v' new: v -> v2 reads v' <- this is why v2 *must* read orig
      - orig: v' new: v -> v2 writes v'' -> orig: v'' new: v''
  c. Finished deploy v2, v1 is no longer running
    - possible states:
      - orig: v new: null
      - orig: v new: v
      - orig: v' new: v
    - possible state changes:
      - orig: v new: null -> v2 reads v
      - orig: v new: null -> v2 writes v' -> orig: v' new: v'
      - orig: v new: v -> v2 reads v
      - orig: v new: v -> v2 writes v' -> orig: v' new: v'
      - orig: v' new: v -> v2 reads v'
      - orig: v' new: v -> v2 writes v'' -> orig: v'' new: v''
  d. postdeploy: Background job to sync data in batches: `UPDATE foo SET new = orig WHERE new != orig AND foo.id > x AND foo.id <= x+window`
    - possible states:
      - orig: v new: null
      - orig: v new: v
      - orig: v' new: v
    - possible state changes:
      - orig: v new: null -> v2 reads v
      - orig: v new: null -> v2 writes v' -> orig: v' new: v'
      - orig: v new: null -> job -> orig: v new: v
      - orig: v new: v -> v2 reads v
      - orig: v new: v -> v2 writes v' -> orig: v' new: v'
      - orig: v' new: v -> v2 reads v'
      - orig: v' new: v -> v2 writes v'' -> orig: v'' new: v''
      - orig: v' new: v -> job -> orig: v new: v
  e. background job finishes
    - possible states:
      - orig: v new: v
    - possible state changes:
      - orig: v new: v -> v2 reads v
      - orig: v new: v -> v2 writes v' -> orig: v' new: v'
3. Deploy v3: read: foo.new, write: foo.orig & foo.new
  a. Start deploying v3
    - possible states:
      - orig: v new: v
    - possible state changes:
      - orig: v new: v -> v2 reads v
      - orig: v new: v -> v2 writes v' -> orig: v' new: v'
      - orig: v new: v -> v3 reads v
      - orig: v new: v -> v3 writes v' -> orig: v' new: v' <- v3 *must* write orig otherwise it would be orig: v new: v' and v2 would read v
  b. Finish deploying v3, v2 is no longer running
    - possible states:
      - orig: v new: v
    - possible state changes:
      - orig: v new: v -> v3 reads v
      - orig: v new: v -> v3 writes v' -> orig: v' new: v'
4. Deploy v4: read: foo.new, write: foo.new
  a. Start deploying v4
    - possible states:
      - orig: v new: v
      - orig: v new: v'
    - possible state changes:
      - orig: v new: v -> v3 reads v
      - orig: v new: v -> v3 writes v' -> orig: v' new: v'
      - orig: v new: v -> v4 reads v
      - orig: v new: v -> v4 writes v' -> orig: v new: v'
      - orig: v new: v' -> v3 reads v'
      - orig: v new: v' -> v3 writes v'' -> orig: v'' new: v''
      - orig: v new: v' -> v4 reads v'
      - orig: v new: v' -> v4 writes v'' -> orig: v new: v''
  b. Finish deploying v4, v3 is no longer running
    - possible states:
      - orig: v new: v
      - orig: v new: v'
    - possible state changes:
      - orig: v new: v -> v4 reads v
      - orig: v new: v -> v4 writes v' -> orig: v new: v'
      - orig: v new: v' -> v4 reads v'
      - orig: v new: v' -> v4 writes v'' -> orig: v new: v''
  c. Postdeploy: `ALTER TABLE foo DROP COLUMN old`
    - possible states:
      - new: v
