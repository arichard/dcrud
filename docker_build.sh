#!/bin/sh
set -e

cd -- "$(dirname "$(realpath "$0")")"

CACHE_USER_CACHE="$PWD/.cache/user_cache"
CACHE_GO_PKG="$PWD/.cache/go_pkg"

# Reuse existing caches if possible
if command -v go > /dev/null 2>&1; then
    eval "$(go env | grep GOMODCACHE)"
    if [ -d "$GOMODCACHE" ]; then
	CACHE_GO_PKG="$(dirname "$GOMODCACHE")"
    fi
fi
if [ -d ~/.cache/go-build ] && [ -d ~/.cache/golangci-lint ]; then
    CACHE_USER_CACHE="$HOME/.cache"
fi

mkdir -p "$CACHE_USER_CACHE" "$CACHE_GO_PKG"

# PostgreSQL
docker run --detach --env POSTGRES_HOST_AUTH_METHOD=trust --name dcrud_postgres --rm postgres:14

# CockroachDB
# set --env PGUSER=root for build.sh
# webui at http://localhost:8080
# docker run --detach --name dcrud_postgres --publish 8080:8080 --rm cockroachdb/cockroach:latest-v21.2 start-single-node --insecure --port 5432

# YugabyteDB
# Some timeouts need to be bumped :(
# web console http://localhost:7000
# docker run --detach --name dcrud_postgres --publish 7000:7000 --rm yugabytedb/yugabyte:2.13.1.0-b112 bin/yugabyted start --daemon=false --ysql_port 5432

cleanup() {
    docker stop dcrud_postgres
}
trap cleanup EXIT

# For some reason [::1]:5432 doesn't work, explicitly use 127.0.0.1 instead of localhost, I think docker ipv6 is disabled
docker run --env DCRUD_POSTGRES_FOUND=1 --env DCRUD_POSTGRES_WAIT=1 --env HOME=/tmp --env PGHOST=127.0.0.1 --interactive --name dcrud_golang --network container:dcrud_postgres --rm --tty --user "$(id -u "$USER"):$(id -g "$USER")" --volume "$PWD:/dcrud" --volume "$CACHE_USER_CACHE:/tmp/.cache" --volume "$CACHE_GO_PKG:/go/pkg" golang:1.19 /dcrud/build.sh
