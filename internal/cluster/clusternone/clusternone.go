package clusternone

import (
	"context"
	"encoding/json"

	"codeberg.org/arichard/dcrud/internal/cluster"
	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dlog"
)

type clusterNone struct{}

var _ cluster.Cluster = clusterNone{}

func New() cluster.Cluster {
	return clusterNone{}
}

func (c clusterNone) AddAnnotation(_ context.Context, _, _ string) error {
	// Can't ever read this so no need to save it
	return nil
}

func (c clusterNone) Instances(_ context.Context, _ string) ([]*cluster.Instance, error) {
	return []*cluster.Instance{{
		Name: "localhost",
	}}, nil
}

func (c clusterNone) TryLock(_ context.Context, _ dlog.Logger, _, _ string) (context.Context, context.CancelFunc, error) {
	ctx, cancel := context.WithCancel(context.Background())
	return ctx, cancel, nil
}

func (c clusterNone) ConfigWatch(ctx context.Context, _ string, f func(json.RawMessage)) (err error) {
	f(json.RawMessage(`{}`))
	<-ctx.Done()
	return derrors.Wrap(ctx.Err(), 0)
}
