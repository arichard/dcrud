package clusterk8s

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"net/url"
	"os"
	"time"

	"codeberg.org/arichard/dcrud/internal/cluster"
	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dlog"
	"codeberg.org/arichard/dcrud/internal/k8s"
)

const (
	lockDuration = time.Minute
	lockSkew     = time.Second
	renewTime    = 5 * time.Second
)

type clusterK8s struct {
	kube     k8s.K8s
	hostname string
}

var _ cluster.Cluster = &clusterK8s{}

type lockData struct {
	Expires time.Time `json:"expires"`
	Owner   string    `json:"owner"`
}

func New(kube k8s.K8s) (cluster.Cluster, error) {
	clstr := clusterK8s{
		kube: kube,
	}
	var err error
	if clstr.hostname, err = os.Hostname(); err != nil {
		return nil, derrors.Wrap(err, 0)
	}
	return &clstr, nil
}

func (c *clusterK8s) AddAnnotation(ctx context.Context, key, value string) error {
	var buf bytes.Buffer
	_, err := c.kube.PodAnnotate(ctx, &buf, c.kube.CurrentNamespace(), c.hostname, "", key, &value)
	if err != nil {
		return derrors.Wrap(err, 0)
	}
	return nil
}

func (c *clusterK8s) Instances(ctx context.Context, appName string) ([]*cluster.Instance, error) {
	var buf bytes.Buffer
	podList, err := c.kube.PodList(ctx, &buf, c.kube.CurrentNamespace(), "app="+appName)
	if err != nil {
		return nil, derrors.Wrap(err, 0)
	}
	instances := make([]*cluster.Instance, len(podList.Items))
	for i, pod := range podList.Items {
		instances[i] = &cluster.Instance{
			Annotations: pod.Metadata.Annotations,
			Name:        pod.Metadata.Name,
		}
	}
	return instances, nil
}

func (c *clusterK8s) TryLock(ctx context.Context, logger dlog.Logger, deploymentName, lockName string) (context.Context, context.CancelFunc, error) {
	lockKey := "org.codeberg.arichard.dcrud.lock." + url.PathEscape(lockName)
	var buf bytes.Buffer
	locked := false
	resourceVersion, lockValue, err := c.annotate(ctx, &buf, deploymentName, lockKey, "", nil, func(lockValue *lockData) (*lockData, bool) {
		now := time.Now().UTC()
		if lockValue != nil && lockValue.Expires.Add(lockSkew).After(now) {
			locked = true
			return nil, false
		}
		return &lockData{
			Expires: now.Add(lockDuration),
			Owner:   c.hostname,
		}, true
	})
	if err != nil {
		return nil, nil, err
	}
	if locked {
		return nil, nil, nil
	}
	var cancel context.CancelFunc
	ctx, cancel = context.WithCancel(ctx)
	go c.holdLock(ctx, cancel, logger, &buf, deploymentName, lockName, lockKey, resourceVersion, lockValue)
	return ctx, cancel, nil
}

func (c *clusterK8s) ConfigWatch(ctx context.Context, configMapName string, callback func(json.RawMessage)) (err error) {
	return derrors.Wrap(c.kube.ConfigWatch(ctx, c.kube.CurrentNamespace(), configMapName, callback), 0)
}

func (c *clusterK8s) holdLock(ctx context.Context, cancel context.CancelFunc, logger dlog.Logger, buf *bytes.Buffer, deploymentName, lockName, lockKey, resourceVersion string, lockValue *lockData) { //nolint:lll // Lots of arguments
	defer func() {
		if _, _, err := c.annotate(context.Background(), buf, deploymentName, lockKey, resourceVersion, lockValue, func(actualLockValue *lockData) (*lockData, bool) {
			return nil, actualLockValue.Expires.Equal(lockValue.Expires) && actualLockValue.Owner == c.hostname
		}); err != nil {
			derrors.LogErr(logger, err)
		}
	}()
	defer cancel()
	for {
		timer := time.NewTimer(time.Until(lockValue.Expires) - renewTime)
		select {
		case <-ctx.Done():
			timer.Stop()
			return
		case <-timer.C:
		}
		timer.Stop()
		owned := true
		newResourceVersion, newLockValue, err := c.annotate(ctx, buf, deploymentName, lockKey, resourceVersion, lockValue, func(actualLockValue *lockData) (*lockData, bool) {
			owned = actualLockValue.Expires.Equal(lockValue.Expires) && actualLockValue.Owner == c.hostname
			return &lockData{
				Expires: time.Now().UTC().Add(lockDuration),
				Owner:   c.hostname,
			}, owned
		})
		if err != nil {
			derrors.LogErr(logger, err)
			return
		}
		if !owned {
			// DR Think about the defer, it doesn't need to do anything but likely will
			logger.Printf("lock lost: %s", lockName)
			return
		}
		resourceVersion = newResourceVersion
		lockValue = newLockValue
	}
}

func (c *clusterK8s) annotate(ctx context.Context, buf *bytes.Buffer, deploymentName, lockKey, resourceVersion string, lockValue *lockData, eval func(*lockData) (*lockData, bool)) (resourceVersionRet string, lockValueRet *lockData, err error) { //nolint:lll // Lots of arguments
	for idx := 0; ; idx++ {
		if resourceVersion == "" {
			var deployment *k8s.Deployment
			if deployment, err = c.kube.DeploymentRead(ctx, c.kube.CurrentNamespace(), deploymentName); err != nil {
				return "", nil, derrors.Wrap(err, 0)
			}
			resourceVersion = deployment.Metadata.ResourceVersion
			if annotation, ok := deployment.Metadata.Annotations[lockKey]; !ok {
				lockValue = nil
			} else {
				var lv lockData
				if err = json.Unmarshal([]byte(annotation), &lv); err != nil {
					return "", nil, derrors.Wrap(err, 0)
				}
				lockValue = &lv
			}
		}
		newLockValue, ok := eval(lockValue)
		if !ok {
			return resourceVersion, lockValue, nil
		}
		var newLockValueString *string
		if newLockValue != nil {
			buf.Reset()
			if err = json.NewEncoder(buf).Encode(newLockValue); err != nil {
				return "", nil, derrors.Wrap(err, 0)
			}
			s := buf.String()
			newLockValueString = &s
		}
		var newResourceVersion string
		if newResourceVersion, err = c.kube.DeploymentAnnotate(ctx, buf, c.kube.CurrentNamespace(), deploymentName, resourceVersion, lockKey, newLockValueString); err == nil {
			return newResourceVersion, newLockValue, nil
		}
		if idx > 3 || !errors.Is(err, k8s.AnnotateConflict) {
			return "", nil, derrors.Wrap(err, 0)
		}
		// Force the deployment to be reread
		resourceVersion = ""
	}
}
