package cluster

import (
	"context"
	"encoding/json"

	"codeberg.org/arichard/dcrud/internal/dlog"
)

type Instance struct {
	Annotations map[string]string `json:"annotations"`
	Name        string            `json:"name"`
}

type Cluster interface {
	AddAnnotation(ctx context.Context, key, value string) error
	Instances(ctx context.Context, appName string) ([]*Instance, error)
	TryLock(ctx context.Context, logger dlog.Logger, deploymentName, lockName string) (context.Context, context.CancelFunc, error)
	ConfigWatch(ctx context.Context, configMapName string, callback func(json.RawMessage)) (err error)
}
