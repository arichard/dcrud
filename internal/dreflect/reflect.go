package dreflect

import (
	"bytes"
	"reflect"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dname"
	"codeberg.org/arichard/dcrud/internal/metamodel"
)

func AddType(model *metamodel.Model, typ reflect.Type, buf *bytes.Buffer) (metamodel.Type, error) {
	var err error
	kind := typ.Kind()
	switch kind {
	case reflect.Complex64, reflect.Complex128, reflect.Array, reflect.Chan, reflect.Func, reflect.Interface, reflect.UnsafePointer:
		// Unsupported kind
		return nil, derrors.Errorf("unsupported kind %v", kind)
	case reflect.Pointer:
		// Skip pointers
		return AddType(model, typ.Elem(), buf)
	case reflect.Map:
		var mapType metamodel.MapType
		if mapType.Key, err = AddType(model, typ.Key(), buf); err != nil {
			return nil, err
		}
		if mapType.Value, err = AddType(model, typ.Elem(), buf); err != nil {
			return nil, err
		}
		mapType.GoType = metamodel.MapOf(mapType.Key.GetGoType(), mapType.Value.GetGoType())
		goType := mapType.GoType.String()
		for _, mt := range model.Maps {
			if mt.GoType.String() == goType {
				return mt, nil
			}
		}
		model.Maps = append(model.Maps, &mapType)
		return &mapType, nil
	case reflect.Slice:
		var arrayType metamodel.ArrayType
		if arrayType.Element, err = AddType(model, typ.Elem(), buf); err != nil {
			return nil, err
		}
		arrayType.GoType = metamodel.SliceOf(arrayType.Element.GetGoType())
		goType := arrayType.GoType.String()
		for _, at := range model.Arrays {
			if at.GoType.String() == goType {
				return at, nil
			}
		}
		model.Arrays = append(model.Arrays, &arrayType)
		return &arrayType, nil
	case reflect.Struct:
		numFields := typ.NumField()
		name := typ.Name()
		structType := metamodel.StructType{
			Basic: metamodel.Basic{
				Name:   dname.ToSnakeCase(name, buf),
				GoType: metamodel.StructOf(typ.PkgPath(), name),
			},
			Fields: make([]*metamodel.StructField, 0, numFields),
		}
		for idx := 0; idx < numFields; idx++ {
			field := typ.Field(idx)
			structField := metamodel.StructField{
				Name: dname.ToSnakeCase(field.Name, buf),
			}
			if structField.Type, err = AddType(model, field.Type, buf); err != nil {
				return nil, err
			}
			structType.Fields = append(structType.Fields, &structField)
		}
		for _, st := range model.Structs {
			// DR Does this test provide sufficient type equality confidence?
			if st.Name == structType.Name {
				return st, nil
			}
		}
		model.Structs = append(model.Structs, &structType)
		return &structType, nil
	default:
		// Make sure this type doesn't exist already
		name := typ.String()
		for _, pt := range model.Primitives {
			if pt.GoType.String() == name {
				return pt, nil
			}
		}
		primitiveType := metamodel.PrimitiveType{
			Basic: metamodel.Basic{
				Name:   dname.ToSnakeCase(name, buf),
				GoType: metamodel.Primitive(name),
			},
		}
		model.Primitives = append(model.Primitives, &primitiveType)
		return &primitiveType, nil
	}
}
