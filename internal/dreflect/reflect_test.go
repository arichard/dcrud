package dreflect_test

import (
	"bytes"
	"reflect"
	"testing"
	"time"

	"codeberg.org/arichard/dcrud/internal/dreflect"
	"codeberg.org/arichard/dcrud/internal/metamodel"
	"codeberg.org/arichard/dcrud/internal/metamodel/mmtd"
	"github.com/stretchr/testify/require"
)

type foo struct {
	Map   map[string]int32
	Attr  string
	Slice []string
	ID    int64
}

func TestAddType(t *testing.T) {
	t.Parallel()
	model := metamodel.Model{
		Primitives: []*metamodel.PrimitiveType{
			&mmtd.Int32,
			&mmtd.Int64,
			&mmtd.String,
		},
		Arrays: []*metamodel.ArrayType{
			&mmtd.Strings,
		},
		Maps: []*metamodel.MapType{
			&mmtd.MapStringInt32,
		},
	}
	typ, err := dreflect.AddType(&model, reflect.TypeOf(&foo{}), &bytes.Buffer{})
	require.NoError(t, err)
	require.NotNil(t, typ)
	require.Len(t, model.Primitives, 3)
	require.Len(t, model.Arrays, 1)
	require.Len(t, model.Maps, 1)
	require.Len(t, model.Structs, 1)
	require.Equal(t, &metamodel.StructType{
		Basic: metamodel.Basic{
			Name:   "foo",
			GoType: metamodel.StructOf("codeberg.org/arichard/dcrud/internal/dreflect_test", "foo"),
		},
		Fields: []*metamodel.StructField{{
			Name: "map",
			Type: &mmtd.MapStringInt32,
		}, {
			Name: "attr",
			Type: &mmtd.String,
		}, {
			Name: "slice",
			Type: &mmtd.Strings,
		}, {
			Name: "id",
			Type: &mmtd.Int64,
		}},
	}, typ)
	require.Equal(t, typ, model.Structs[0])
}

func TestAddTypeEmptyModel(t *testing.T) {
	t.Parallel()
	var model metamodel.Model
	typ, err := dreflect.AddType(&model, reflect.TypeOf(&foo{}), &bytes.Buffer{})
	require.NoError(t, err)
	require.NotNil(t, typ)
	require.Len(t, model.Primitives, 3)
	require.Len(t, model.Arrays, 1)
	require.Len(t, model.Maps, 1)
	require.Len(t, model.Structs, 1)
	i32 := mmtd.Int32
	i32.PostgresType = ""
	i32.PostgresSerialType = ""
	i64 := mmtd.Int64
	i64.PostgresType = ""
	i64.PostgresSerialType = ""
	str := mmtd.String
	str.PostgresType = ""
	require.Equal(t, &metamodel.StructType{
		Basic: metamodel.Basic{
			Name:   "foo",
			GoType: metamodel.StructOf("codeberg.org/arichard/dcrud/internal/dreflect_test", "foo"),
		},
		Fields: []*metamodel.StructField{{
			Name: "map",
			Type: &metamodel.MapType{
				Basic: metamodel.Basic{
					GoType: metamodel.MapOf(str.GoType, i32.GoType),
				},
				Key:   &str,
				Value: &i32,
			},
		}, {
			Name: "attr",
			Type: &str,
		}, {
			Name: "slice",
			Type: &metamodel.ArrayType{
				Basic: metamodel.Basic{
					GoType: metamodel.SliceOf(str.GoType),
				},
				Element: &str,
			},
		}, {
			Name: "id",
			Type: &i64,
		}},
	}, typ)
	require.Equal(t, typ, model.Structs[0])
}

func TestAddTimeEmptyModel(t *testing.T) {
	t.Parallel()
	var model metamodel.Model
	goTypeTime := reflect.TypeOf(time.Time{})
	typ, err := dreflect.AddType(&model, goTypeTime, &bytes.Buffer{})
	require.NoError(t, err)
	require.NotNil(t, typ)
	require.Len(t, model.Primitives, 6)
	require.Len(t, model.Arrays, 2)
	require.Len(t, model.Maps, 0)
	require.Len(t, model.Structs, 4)
	bln := mmtd.Bool
	bln.PostgresType = ""
	i64 := mmtd.Int64
	i64.PostgresType = ""
	i64.PostgresSerialType = ""
	str := mmtd.String
	str.PostgresType = ""
	zone := metamodel.StructType{
		Basic: metamodel.Basic{
			Name:   "zone",
			GoType: metamodel.StructOf("time", "zone"),
		},
		Fields: []*metamodel.StructField{{
			Name: "name",
			Type: &str,
		}, {
			Name: "offset",
			Type: &mmtd.Int,
		}, {
			Name: "is_dst",
			Type: &bln,
		}},
	}
	zoneArray := metamodel.ArrayType{
		Basic: metamodel.Basic{
			GoType: metamodel.SliceOf(zone.GoType),
		},
		Element: &zone,
	}
	zoneTrans := metamodel.StructType{
		Basic: metamodel.Basic{
			Name:   "zone_trans",
			GoType: metamodel.StructOf("time", "zoneTrans"),
		},
		Fields: []*metamodel.StructField{{
			Name: "when",
			Type: &i64,
		}, {
			Name: "index",
			Type: &mmtd.Uint8,
		}, {
			Name: "isstd",
			Type: &bln,
		}, {
			Name: "isutc",
			Type: &bln,
		}},
	}
	zoneTransArray := metamodel.ArrayType{
		Basic: metamodel.Basic{
			GoType: metamodel.SliceOf(zoneTrans.GoType),
		},
		Element: &zoneTrans,
	}
	location := metamodel.StructType{
		Basic: metamodel.Basic{
			Name:   "location",
			GoType: metamodel.StructOf("time", "Location"),
		},
		Fields: []*metamodel.StructField{{
			Name: "name",
			Type: &str,
		}, {
			Name: "zone",
			Type: &zoneArray,
		}, {
			Name: "tx",
			Type: &zoneTransArray,
		}, {
			Name: "extend",
			Type: &str,
		}, {
			Name: "cache_start",
			Type: &i64,
		}, {
			Name: "cache_end",
			Type: &i64,
		}, {
			Name: "cache_zone",
			Type: &zone,
		}},
	}
	expected := metamodel.StructType{
		Basic: metamodel.Basic{
			Name:   "time",
			GoType: metamodel.StructOf("time", "Time"),
		},
		Fields: []*metamodel.StructField{{
			Name: "wall",
			Type: &mmtd.Uint64,
		}, {
			Name: "ext",
			Type: &i64,
		}, {
			Name: "loc",
			Type: &location,
		}},
	}
	require.Equal(t, &expected, typ)
	require.Equal(t, typ, model.Structs[3])
}
