package foo

import (
	"bytes"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"time"

	"codeberg.org/arichard/dcrud/internal/cluster"
	"codeberg.org/arichard/dcrud/internal/cluster/clusterk8s"
	"codeberg.org/arichard/dcrud/internal/cluster/clusternone"
	"codeberg.org/arichard/dcrud/internal/dconfig"
	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dio"
	"codeberg.org/arichard/dcrud/internal/dlog"
	"codeberg.org/arichard/dcrud/internal/dsql"
	"codeberg.org/arichard/dcrud/internal/gen/expected"
	"codeberg.org/arichard/dcrud/internal/k8s"
	"codeberg.org/arichard/dcrud/internal/k8s/k8sbasic"
	"codeberg.org/arichard/dcrud/internal/metamodel"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gopkg.in/yaml.v3"
)

func yamlDecodeFile(name string, out any) (err error) {
	var f *os.File
	if f, err = os.Open(name); err != nil {
		return derrors.Wrap(err, 0)
	}
	defer dio.CloseHelper(f, &err)
	if err = yaml.NewDecoder(f).Decode(out); err != nil {
		return derrors.Wrap(err, 0)
	}
	return nil
}

func parseArgs(args []string) (string, error) {
	cmdLine := flag.NewFlagSet(args[0], flag.ContinueOnError)
	var configFile string
	cmdLine.StringVar(&configFile, "c", "", "yaml config file")
	if err := cmdLine.Parse(args[1:]); err != nil {
		if errors.Is(err, flag.ErrHelp) {
			return "", derrors.ExitErrorWrap(0, err)
		}
		return "", derrors.ExitErrorWrap(1, err)
	}
	if extraArgs := cmdLine.Args(); len(extraArgs) > 0 {
		fmt.Fprintf(cmdLine.Output(), "unexpected additional args: %v\n", extraArgs)
		cmdLine.Usage()
		return "", derrors.ExitErrorWrap(1, nil)
	}
	return configFile, nil
}

func Bar(ctx context.Context, logger dlog.Logger, args []string) (err error) {
	var configFile string
	if configFile, err = parseArgs(args); err != nil {
		return err
	}
	cfg := dconfig.Config{
		PG: &dconfig.Postgres{
			Username: "postgres",
			DBName:   "dcrud",
		},
	}
	if configFile != "" {
		if err = yamlDecodeFile(configFile, &cfg); err != nil {
			return err
		}
	}

	pgxLogger, ok := logger.(pgx.Logger)
	if !ok {
		pgxLogger = dsql.NewPgxLoggerWrapper(logger)
	}

	var model metamodel.Model
	if err = model.Load(metamodel.DefaultFS); err != nil {
		return err
	}
	if err = model.Load(expected.ModelFS); err != nil {
		return err
	}

	if err = dsql.CreateDB(ctx, cfg.PG, pgxLogger); err != nil {
		return err
	}
	var conn *pgxpool.Pool
	if conn, err = dsql.ConnectPool(ctx, cfg.PG, pgxLogger); err != nil {
		return err
	}
	defer conn.Close()

	var kube k8s.K8s
	var clstr cluster.Cluster
	if kube, err = k8sbasic.New(); err != nil {
		logger.Print(err.Error())
	} else if clstr, err = clusterk8s.New(kube); err != nil {
		logger.Print(err.Error())
	}
	if clstr == nil {
		logger.Print("unable to create k8s cluster, falling back to none")
		clstr = clusternone.New()
	}

	if err = migrate(ctx, logger, clstr, conn, &model); err != nil {
		return err
	}

	fatalChan := make(chan error)
	var srvr *expected.Server
	if srvr, err = expected.NewServer(logger, conn, clstr, fatalChan); err != nil {
		return err
	}
	defer func() {
		cctx, cancel := context.WithTimeout(context.Background(), 5*time.Second) //nolint:gomnd // meh
		defer cancel()
		dio.CloseCtxHelper(cctx, srvr, &err)
	}()

	select {
	case <-ctx.Done():
		return nil
	case err = <-fatalChan:
		return err
	}
}

func migrate(ctx context.Context, logger dlog.Logger, clstr cluster.Cluster, conn *pgxpool.Pool, dcrudModel *metamodel.Model) error {
	var cancel context.CancelFunc
	var err error
	ctx, cancel, err = clstr.TryLock(ctx, logger, "dcrud", "schema")
	if err != nil {
		return derrors.Wrap(err, 0)
	}
	defer cancel()
	sqlModel := metamodel.Model{
		Primitives: make([]*metamodel.PrimitiveType, len(dcrudModel.Primitives)),
		Arrays:     make([]*metamodel.ArrayType, len(dcrudModel.Arrays)),
		Maps:       make([]*metamodel.MapType, len(dcrudModel.Maps)),
	}
	copy(sqlModel.Primitives, dcrudModel.Primitives)
	copy(sqlModel.Arrays, dcrudModel.Arrays)
	copy(sqlModel.Maps, dcrudModel.Maps)
	var buf bytes.Buffer
	var tables []*metamodel.StructType
	if tables, err = dsql.AddTables(ctx, conn, &sqlModel, &buf); err != nil {
		return err
	}
	tableMap := make(map[string]struct{}, len(tables))
	for _, s := range sqlModel.Structs {
		tableMap[s.Name] = struct{}{}
	}
	for _, s := range dcrudModel.Structs {
		if _, ok := tableMap[s.Name]; !ok {
			if s.Name == "foo_bop" {
				if err = expected.FooBopCreateTable(ctx, conn); err != nil {
					return err
				}
			}
		}
	}

	/*
	 * crud annotations on the current instance
	 * get all annotations and their instances
	 * obtain a lock
	 * release the lock
	 *
	 * Is the pod's version visible? If not, set it
	 * On startup, obtain the schema lock.
	 *   Inspect the schema version, if it's missing, create tables, if they already exist, fail
	 *   If schema version is newer, verify (somehow) it's compatible
	 *   If schema version is older, begin migration
	 */

	return nil
}
