package foo

import (
	"errors"
	"testing"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"github.com/stretchr/testify/require"
)

func TestParseArgs(t *testing.T) {
	t.Parallel()
	exitCodeZero := 0
	exitCodeFail := 1
	testCases := []struct {
		expectedExitCode   *int
		name               string
		expectedConfigFile string
		args               []string
	}{{
		args: []string{"TestParseArgs"},
	}, {
		expectedExitCode: &exitCodeZero,
		args:             []string{"TestParseArgs", "-h"},
	}, {
		expectedExitCode: &exitCodeFail,
		args:             []string{"TestParseArgs", "-invalid"},
	}, {
		expectedExitCode: &exitCodeFail,
		args:             []string{"TestParseArgs", "invalid"},
	}, {
		expectedExitCode: &exitCodeFail,
		args:             []string{"TestParseArgs", "-c"},
	}, {
		expectedConfigFile: "config.yaml",
		args:               []string{"TestParseArgs", "-c", "config.yaml"},
	}, {
		expectedConfigFile: "config.yaml",
		args:               []string{"TestParseArgs", "-c=config.yaml"},
	}, {
		expectedExitCode: &exitCodeFail,
		args:             []string{"TestParseArgs", "-c", "config.yaml", "invalid"},
	}}
	for i := range testCases {
		testCase := &testCases[i]
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			configFile, err := parseArgs(testCase.args)
			if testCase.expectedExitCode == nil {
				require.NoError(t, err)
				require.Equal(t, testCase.expectedConfigFile, configFile)
			} else {
				var ece derrors.ExitCodeError
				require.True(t, errors.As(err, &ece))
				require.Equal(t, *testCase.expectedExitCode, ece.ExitCode())
			}
		})
	}
}
