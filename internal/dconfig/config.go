package dconfig

type Config struct {
	PG *Postgres `yaml:"postgres"`
}

type Postgres struct {
	Params   map[string]string `yaml:"params"`
	Username string            `yaml:"username"`
	Password string            `yaml:"password"`
	DBName   string            `yaml:"db_name"`
	Hosts    []string          `yaml:"hosts"`
}
