package metamodel

type PrimitiveType struct {
	Basic `yaml:",inline"`
}

var _ Type = &PrimitiveType{}

type serialPrimitiveType struct {
	serialBasic `yaml:",inline"`
}

func (spt *serialPrimitiveType) asType(pt *PrimitiveType) {
	spt.serialBasic.asType(&pt.Basic)
}

func (pt *PrimitiveType) isType() {}

func (pt *PrimitiveType) GetPostgresType() string {
	return pt.PostgresType
}

func (pt *PrimitiveType) GetPostgresSerialType() string {
	if pt.PostgresSerialType != "" {
		return pt.PostgresSerialType
	}
	return pt.GetPostgresType()
}
