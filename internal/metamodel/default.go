package metamodel

import "embed"

//go:embed primitives
//go:embed arrays
//go:embed structs
var DefaultFS embed.FS
