package mmtd

import (
	"embed"

	"codeberg.org/arichard/dcrud/internal/metamodel"
)

//nolint:gochecknoglobals // Don't care about globals in test data
var (
	//go:embed maps
	//go:embed structs
	ModelFS embed.FS
	Byte    = metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:   "byte",
			GoType: metamodel.Byte,
		},
	}
	Bool = metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:         "bool",
			GoType:       metamodel.Bool,
			PostgresType: "boolean",
		},
	}
	Int = metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:   "int",
			GoType: metamodel.Int,
		},
	}
	Int16 = metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:               "int16",
			GoType:             metamodel.Int16,
			PostgresType:       "smallint",
			PostgresSerialType: "smallserial",
		},
	}
	Int32 = metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:               "int32",
			GoType:             metamodel.Int32,
			PostgresType:       "integer",
			PostgresSerialType: "serial",
		},
	}
	Int64 = metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:               "int64",
			GoType:             metamodel.Int64,
			PostgresType:       "bigint",
			PostgresSerialType: "bigserial",
		},
	}
	Uint8 = metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:   "uint8",
			GoType: metamodel.Uint8,
		},
	}
	Uint64 = metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:   "uint64",
			GoType: metamodel.Uint64,
		},
	}
	Float32 = metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:         "float32",
			GoType:       metamodel.Float32,
			PostgresType: "real",
		},
	}
	Float64 = metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:         "float64",
			GoType:       metamodel.Float64,
			PostgresType: "double precision",
		},
	}
	String = metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:         "string",
			GoType:       metamodel.String,
			PostgresType: "text",
		},
	}
	Bytes = metamodel.ArrayType{
		Basic: metamodel.Basic{
			Name:         "bytes",
			GoType:       metamodel.SliceOf(Byte.GoType),
			PostgresType: "bytea",
		},
		Element: &Byte,
	}
	Strings = metamodel.ArrayType{
		Basic: metamodel.Basic{
			Name:   "strings",
			GoType: metamodel.SliceOf(String.GoType),
		},
		Element: &String,
	}
	MapStringString = metamodel.MapType{
		Basic: metamodel.Basic{
			Name:   "map_string_string",
			GoType: metamodel.MapOf(String.GoType, String.GoType),
		},
		Key:   &String,
		Value: &String,
	}
	MapStringInt32 = metamodel.MapType{
		Basic: metamodel.Basic{
			Name:   "map_string_int32",
			GoType: metamodel.MapOf(String.GoType, Int32.GoType),
		},
		Key:   &String,
		Value: &Int32,
	}
	Time = metamodel.StructType{
		Basic: metamodel.Basic{
			Name:         "time",
			GoType:       metamodel.StructOf("time", "Time"),
			PostgresType: "timestamp with time zone",
		},
		Fields: []*metamodel.StructField{{
			Name: "foo",
			Type: &Bytes,
		}},
		PrimaryKey: []*metamodel.StructField{},
		Uniques:    [][]*metamodel.StructField{},
	}
	Struct = metamodel.StructType{
		Basic: metamodel.Basic{
			Name: "struct",
		},
	}
	Bar = metamodel.StructType{
		Basic: metamodel.Basic{
			Name: "bar",
		},
		Fields: []*metamodel.StructField{{
			Name: "id",
			Type: &Int64,
		}, {
			Name: "bar",
			Type: &String,
		}},
		PrimaryKey: []*metamodel.StructField{},
		Uniques:    [][]*metamodel.StructField{},
	}
)
