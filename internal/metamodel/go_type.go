package metamodel

import "bytes"

//nolint:gochecknoglobals // Effectively consts, Type is immutable, but go doesn't permit structs to be defined const
var (
	Bool       = Primitive("bool")
	Int        = Primitive("int")
	Int8       = Primitive("int8")
	Int16      = Primitive("int16")
	Int32      = Primitive("int32")
	Int64      = Primitive("int64")
	Uint       = Primitive("uint")
	Uint8      = Primitive("uint8")
	Byte       = Primitive("byte")
	Uint16     = Primitive("uint16")
	Uint32     = Primitive("uint32")
	Uint64     = Primitive("uint64")
	Uintptr    = Primitive("uintptr")
	Float32    = Primitive("float32")
	Float64    = Primitive("float64")
	Complex64  = Primitive("complex64")
	Complex128 = Primitive("complex128")
	String     = Primitive("string")
)

type GoType interface {
	Elem() GoType
	Key() GoType
	Name(buf *bytes.Buffer)
	PkgPath() string
	String() string
	Variant() Variant
	isGoType()
}

type Variant int

const (
	VariantInvalid Variant = iota
	VariantBasic
	VariantPointer
	VariantSlice
	VariantMap
	VariantStruct
)

type serialGoType struct {
	Element *serialGoType `yaml:"element,omitempty"`
	Key     *serialGoType `yaml:"key,omitempty"`
	Pointer *serialGoType `yaml:"pointer,omitempty"`
	Package string        `yaml:"package,omitempty"`
	Name    string        `yaml:"name,omitempty"`
}

type basicGoType struct {
	name string
}

var _ GoType = &basicGoType{}

type pointerGoType struct {
	elem GoType
}

var _ GoType = &pointerGoType{}

type sliceGoType struct {
	elem GoType
}

var _ GoType = &sliceGoType{}

type mapGoType struct {
	key  GoType
	elem GoType
}

var _ GoType = &mapGoType{}

type structGoType struct {
	pkg  string
	name string
}

var _ GoType = &structGoType{}

func (sgt *serialGoType) buildGoType() GoType {
	if sgt.Pointer != nil {
		return PointerTo(sgt.Pointer.buildGoType())
	}
	if sgt.Element != nil {
		if sgt.Key != nil {
			return MapOf(sgt.Key.buildGoType(), sgt.Element.buildGoType())
		}
		return SliceOf(sgt.Element.buildGoType())
	}
	if sgt.Package != "" {
		return StructOf(sgt.Package, sgt.Name)
	}
	switch sgt.Name {
	case "bool":
		return Bool
	case "int":
		return Int
	case "int8":
		return Int8
	case "int16":
		return Int16
	case "int32":
		return Int32
	case "int64":
		return Int64
	case "uint":
		return Uint
	case "uint8":
		return Uint8
	case "byte":
		return Byte
	case "uint16":
		return Uint16
	case "uint32":
		return Uint32
	case "uint64":
		return Uint64
	case "uintptr":
		return Uintptr
	case "float32":
		return Float32
	case "float64":
		return Float64
	case "complex64":
		return Complex64
	case "complex128":
		return Complex128
	case "string":
		return String
	default:
		return nil
	}
}

func Primitive(name string) GoType {
	return &basicGoType{name: name}
}

func (bgt *basicGoType) isGoType() {}

func (bgt *basicGoType) Elem() GoType {
	panic("reflect: Elem of invalid type basic")
}

func (bgt *basicGoType) Key() GoType {
	panic("reflect: Key of non-map type basic")
}

func (bgt *basicGoType) Name(buf *bytes.Buffer) {
	buf.WriteString(bgt.name)
}

func (bgt *basicGoType) PkgPath() string {
	return ""
}

func (bgt *basicGoType) String() string {
	return bgt.name
}

func (bgt *basicGoType) Variant() Variant {
	return VariantBasic
}

func PointerTo(elem GoType) GoType {
	return &pointerGoType{elem: elem}
}

func (pgt *pointerGoType) isGoType() {}

func (pgt *pointerGoType) Elem() GoType {
	return pgt.elem
}

func (pgt *pointerGoType) Key() GoType {
	panic("reflect: Key of non-map type pointer")
}

func (pgt *pointerGoType) Name(buf *bytes.Buffer) {
	buf.WriteByte('*')
	pgt.elem.Name(buf)
}

func (pgt *pointerGoType) PkgPath() string {
	return ""
}

func (pgt *pointerGoType) String() string {
	var buf bytes.Buffer
	pgt.Name(&buf)
	return buf.String()
}

func (pgt *pointerGoType) Variant() Variant {
	return VariantPointer
}

func SliceOf(elem GoType) GoType {
	return &sliceGoType{elem: elem}
}

func (sgt *sliceGoType) isGoType() {}

func (sgt *sliceGoType) Elem() GoType {
	return sgt.elem
}

func (sgt *sliceGoType) Key() GoType {
	panic("reflect: Key of non-map type slice")
}

func (sgt *sliceGoType) Name(buf *bytes.Buffer) {
	buf.WriteString("[]")
	sgt.elem.Name(buf)
}

func (sgt *sliceGoType) PkgPath() string {
	return ""
}

func (sgt *sliceGoType) String() string {
	var buf bytes.Buffer
	sgt.Name(&buf)
	return buf.String()
}

func (sgt *sliceGoType) Variant() Variant {
	return VariantSlice
}

func MapOf(key, elem GoType) GoType {
	return &mapGoType{
		key:  key,
		elem: elem,
	}
}

func (mgt *mapGoType) isGoType() {}

func (mgt *mapGoType) Elem() GoType {
	return mgt.elem
}

func (mgt *mapGoType) Key() GoType {
	return mgt.key
}

func (mgt *mapGoType) Name(buf *bytes.Buffer) {
	buf.WriteString("map[")
	mgt.key.Name(buf)
	buf.WriteByte(']')
	mgt.elem.Name(buf)
}

func (mgt *mapGoType) PkgPath() string {
	return ""
}

func (mgt *mapGoType) String() string {
	var buf bytes.Buffer
	mgt.Name(&buf)
	return buf.String()
}

func (mgt *mapGoType) Variant() Variant {
	return VariantMap
}

func StructOf(pkg, name string) GoType {
	return &structGoType{
		pkg:  pkg,
		name: name,
	}
}

func (sgt *structGoType) isGoType() {}

func (sgt *structGoType) Elem() GoType {
	panic("reflect: Elem of invalid type struct")
}

func (sgt *structGoType) Key() GoType {
	panic("reflect: Key of non-map type struct")
}

func (sgt *structGoType) Name(buf *bytes.Buffer) {
	if sgt.pkg != "" {
		buf.WriteString(sgt.pkg)
		buf.WriteByte('.')
	}
	buf.WriteString(sgt.name)
}

func (sgt *structGoType) PkgPath() string {
	return sgt.pkg
}

func (sgt *structGoType) String() string {
	var buf bytes.Buffer
	sgt.Name(&buf)
	return buf.String()
}

func (sgt *structGoType) Variant() Variant {
	return VariantStruct
}
