package metamodel

type EmbedType struct {
	Fields []*StructField
	Basic
}

var _ Type = &EmbedType{}

func (et *EmbedType) isType() {}

func (et *EmbedType) GetPostgresType() string {
	if et.PostgresType != "" {
		return et.PostgresType
	}
	return jsonb
}

func (et *EmbedType) GetPostgresSerialType() string {
	if et.PostgresSerialType != "" {
		return et.PostgresSerialType
	}
	return et.GetPostgresType()
}
