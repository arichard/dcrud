package metamodel

type ArrayType struct {
	Element Type
	Basic
}

var _ Type = &ArrayType{}

type serialArrayType struct {
	Element     string `yaml:"element"`
	serialBasic `yaml:",inline"`
}

func (sat *serialArrayType) asType(ldr *loader, at *ArrayType) {
	sat.serialBasic.asType(&at.Basic)
	ldr.addUsage(sat.Element, &at.Element)
}

func (at *ArrayType) isType() {}

func (at *ArrayType) GetPostgresType() string {
	if at.PostgresType != "" {
		return at.PostgresType
	}
	return at.Element.GetPostgresType() + "[]"
}

func (at *ArrayType) GetPostgresSerialType() string {
	if at.PostgresSerialType != "" {
		return at.PostgresSerialType
	}
	return at.GetPostgresType()
}
