package metamodel_test

import (
	"testing"

	"codeberg.org/arichard/dcrud/internal/metamodel"
	"codeberg.org/arichard/dcrud/internal/metamodel/mmtd"
	"github.com/stretchr/testify/require"
)

func TestLoad(t *testing.T) {
	t.Parallel()
	var model metamodel.Model
	err := model.Load(metamodel.DefaultFS)
	require.NoError(t, err)
	err = model.Load(mmtd.ModelFS)
	require.NoError(t, err)
	require.ElementsMatch(t, []*metamodel.PrimitiveType{
		&mmtd.Bool,
		&mmtd.Byte,
		&mmtd.Float32,
		&mmtd.Float64,
		&mmtd.Int16,
		&mmtd.Int32,
		&mmtd.Int64,
		&mmtd.String,
	}, model.Primitives)
	require.ElementsMatch(t, []*metamodel.ArrayType{
		&mmtd.Bytes,
		&mmtd.Strings,
	}, model.Arrays)
	require.ElementsMatch(t, []*metamodel.MapType{
		&mmtd.MapStringString,
	}, model.Maps)
	require.ElementsMatch(t, []*metamodel.StructType{
		&mmtd.Bar,
		&mmtd.Time,
	}, model.Structs)
}

func TestGetType(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		typ                  metamodel.Type
		name                 string
		expectedPostgresType string
	}{{
		typ:                  &mmtd.String,
		expectedPostgresType: "text",
	}, {
		typ:                  &mmtd.Strings,
		expectedPostgresType: "text[]",
	}, {
		typ: &metamodel.ArrayType{
			Basic: metamodel.Basic{
				PostgresType: "bytea",
			},
			Element: &mmtd.Byte,
		},
		expectedPostgresType: "bytea",
	}, {
		typ:                  &mmtd.MapStringInt32,
		expectedPostgresType: "jsonb",
	}, {
		typ: &metamodel.MapType{
			Basic: metamodel.Basic{
				PostgresType: "mapPostgresCustom",
			},
			Key:   &metamodel.PrimitiveType{},
			Value: &metamodel.PrimitiveType{},
		},
		expectedPostgresType: "mapPostgresCustom",
	}, {
		typ:                  &mmtd.Struct,
		expectedPostgresType: "jsonb",
	}, {
		typ: &metamodel.StructType{
			Basic: metamodel.Basic{
				Name:         "custom",
				PostgresType: "structPostgresCustom",
			},
		},
		expectedPostgresType: "structPostgresCustom",
	}}
	for i := range testCases {
		testCase := &testCases[i]
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			// DR Namespace scoping?
			require.Equal(t, testCase.expectedPostgresType, testCase.typ.GetPostgresType())
		})
	}
}
