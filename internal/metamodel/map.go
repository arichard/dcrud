package metamodel

type MapType struct {
	Key   Type
	Value Type
	Basic
}

var _ Type = &MapType{}

type serialMapType struct {
	Key         string `yaml:"key"`
	Value       string `yaml:"value"`
	serialBasic `yaml:",inline"`
}

func (smt *serialMapType) asType(ldr *loader, mt *MapType) {
	smt.serialBasic.asType(&mt.Basic)
	ldr.addUsage(smt.Key, &mt.Key)
	ldr.addUsage(smt.Value, &mt.Value)
}

func (mt *MapType) isType() {}

func (mt *MapType) GetPostgresType() string {
	if mt.PostgresType != "" {
		return mt.PostgresType
	}
	return jsonb
}

func (mt *MapType) GetPostgresSerialType() string {
	if mt.PostgresSerialType != "" {
		return mt.PostgresSerialType
	}
	return mt.GetPostgresType()
}
