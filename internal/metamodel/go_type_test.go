package metamodel_test

import (
	"bytes"
	"testing"

	"codeberg.org/arichard/dcrud/internal/metamodel"
	"github.com/stretchr/testify/require"
)

func TestGoType(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		goType          metamodel.GoType
		expectedElem    metamodel.GoType
		expectedKey     metamodel.GoType
		expectedName    string
		expectedPkgPath string
		name            string
		expectedVariant metamodel.Variant
	}{{
		goType:          metamodel.Primitive("asdf"),
		expectedElem:    nil,
		expectedKey:     nil,
		expectedName:    "asdf",
		expectedPkgPath: "",
		expectedVariant: metamodel.VariantBasic,
	}, {
		goType:          metamodel.PointerTo(metamodel.Bool),
		expectedElem:    metamodel.Bool,
		expectedKey:     nil,
		expectedName:    "*bool",
		expectedPkgPath: "",
		expectedVariant: metamodel.VariantPointer,
	}, {
		goType:          metamodel.SliceOf(metamodel.Byte),
		expectedElem:    metamodel.Byte,
		expectedKey:     nil,
		expectedName:    "[]byte",
		expectedPkgPath: "",
		expectedVariant: metamodel.VariantSlice,
	}, {
		goType:          metamodel.MapOf(metamodel.String, metamodel.Int),
		expectedElem:    metamodel.Int,
		expectedKey:     metamodel.String,
		expectedName:    "map[string]int",
		expectedPkgPath: "",
		expectedVariant: metamodel.VariantMap,
	}, {
		goType:          metamodel.StructOf("", "fdsa"),
		expectedElem:    nil,
		expectedKey:     nil,
		expectedName:    "fdsa",
		expectedPkgPath: "",
		expectedVariant: metamodel.VariantStruct,
	}, {
		goType:          metamodel.StructOf("bar", "Foo"),
		expectedElem:    nil,
		expectedKey:     nil,
		expectedName:    "bar.Foo",
		expectedPkgPath: "bar",
		expectedVariant: metamodel.VariantStruct,
	}}
	for i := range testCases {
		testCase := &testCases[i]
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			var buf bytes.Buffer
			testCase.goType.Name(&buf)
			require.Equal(t, testCase.expectedElem, recoverElem(testCase.goType))
			require.Equal(t, testCase.expectedKey, recoverKey(testCase.goType))
			require.Equal(t, testCase.expectedName, buf.String())
			require.Equal(t, testCase.expectedName, testCase.goType.String())
			require.Equal(t, testCase.expectedPkgPath, testCase.goType.PkgPath())
			require.Equal(t, testCase.expectedVariant, testCase.goType.Variant())
		})
	}
}

func recoverElem(gt metamodel.GoType) metamodel.GoType {
	defer func() {
		recover() //nolint:errcheck // Don't care about the result
	}()
	return gt.Elem()
}

func recoverKey(gt metamodel.GoType) metamodel.GoType {
	defer func() {
		recover() //nolint:errcheck // Don't care about the result
	}()
	return gt.Key()
}
