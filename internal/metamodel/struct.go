package metamodel

type StructType struct {
	Fields     []*StructField
	PrimaryKey []*StructField
	Uniques    [][]*StructField
	Basic
}

var _ Type = &StructType{}

type serialStructType struct {
	Fields      []*serialStructField `yaml:"fields"`
	PrimaryKey  []string             `yaml:"primary_key,omitempty"`
	Uniques     [][]string           `yaml:"uniques"`
	serialBasic `yaml:",inline"`
}

type StructField struct {
	Type        Type
	Name        string
	Annotations []string
	NotNull     bool
	ReadOnly    bool
	Serial      bool
}

type serialStructField struct {
	Name        string   `yaml:"name"`
	Type        string   `yaml:"type"`
	Annotations []string `yaml:"annotations,omitempty"`
	NotNull     bool     `yaml:"not_null"`
	ReadOnly    bool     `yaml:"read_only"`
	Serial      bool     `yaml:"serial"`
}

func (sst *serialStructType) asType(ldr *loader, structType *StructType) {
	sst.serialBasic.asType(&structType.Basic)
	structType.Fields = make([]*StructField, len(sst.Fields))
	for idx, ssf := range sst.Fields {
		structField := StructField{
			Name:        ssf.Name,
			Annotations: ssf.Annotations,
			NotNull:     ssf.NotNull,
			ReadOnly:    ssf.ReadOnly,
			Serial:      ssf.Serial,
		}
		ldr.addUsage(ssf.Type, &structField.Type)
		structType.Fields[idx] = &structField
	}
	structType.PrimaryKey = resolveFieldNames(structType, sst.PrimaryKey)
	structType.Uniques = make([][]*StructField, len(sst.Uniques))
	for i, unique := range sst.Uniques {
		structType.Uniques[i] = resolveFieldNames(structType, unique)
	}
}

func resolveFieldNames(st *StructType, names []string) []*StructField {
	fields := make([]*StructField, len(names))
	for i, name := range names {
		for _, sf := range st.Fields {
			if name == sf.Name {
				fields[i] = sf
				break
			}
		}
	}
	return fields
}

func (st *StructType) isType() {}

func (st *StructType) GetPostgresType() string {
	if st.PostgresType != "" {
		return st.PostgresType
	}
	return jsonb
}

func (st *StructType) GetPostgresSerialType() string {
	if st.PostgresSerialType != "" {
		return st.PostgresSerialType
	}
	return st.GetPostgresType()
}
