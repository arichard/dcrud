package metamodel

import (
	"errors"
	"io/fs"
	"path"
	"strings"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dio"
	"gopkg.in/yaml.v3"
)

const (
	isDir      = derrors.StrError("isDir")
	yamlSuffix = ".yaml"
	jsonb      = "jsonb"
)

type Type interface {
	GetAnnotations() []string
	GetGoType() GoType
	GetPostgresSerialType() string
	GetPostgresType() string
	isType()
}

type Basic struct {
	GoType             GoType
	Name               string
	PostgresType       string
	PostgresSerialType string
	Annotations        []string
}

type serialBasic struct {
	GoType             *serialGoType `yaml:"go_type,omitempty"`
	PostgresType       string        `yaml:"postgres_type,omitempty"`
	PostgresSerialType string        `yaml:"postgres_serial_type,omitempty"`
	Annotations        []string      `yaml:"annotations,omitempty"`
}

type Model struct {
	Primitives []*PrimitiveType
	Arrays     []*ArrayType
	Maps       []*MapType
	Structs    []*StructType
}

type resolveData struct {
	declaration Type
	usages      []*Type
}

type loader struct {
	modelFS fs.FS
	resolve map[string]*resolveData
}

func (model *Model) Load(modelFS fs.FS) error {
	ldr := loader{
		modelFS: modelFS,
		resolve: make(map[string]*resolveData),
	}
	for _, primitive := range model.Primitives {
		ldr.getResolveData(primitive.Name).declaration = primitive
	}
	for _, array := range model.Arrays {
		ldr.getResolveData(array.Name).declaration = array
	}
	for _, mp := range model.Maps {
		ldr.getResolveData(mp.Name).declaration = mp
	}
	for _, strct := range model.Structs {
		ldr.getResolveData(strct.Name).declaration = strct
	}
	if err := fs.WalkDir(modelFS, "primitives", func(filename string, entry fs.DirEntry, err error) error {
		var spt serialPrimitiveType
		var pt PrimitiveType
		if err = ldr.walkDir(filename, entry, err, &spt, &pt.Name, &pt); err != nil {
			return filterWalkDirError(err)
		}
		spt.asType(&pt)
		model.Primitives = append(model.Primitives, &pt)
		return nil
	}); err != nil {
		return derrors.Wrap(err, 0)
	}
	if err := fs.WalkDir(modelFS, "arrays", func(filename string, entry fs.DirEntry, err error) error {
		var sat serialArrayType
		var arrayType ArrayType
		if err = ldr.walkDir(filename, entry, err, &sat, &arrayType.Name, &arrayType); err != nil {
			return filterWalkDirError(err)
		}
		sat.asType(&ldr, &arrayType)
		model.Arrays = append(model.Arrays, &arrayType)
		return nil
	}); err != nil {
		return derrors.Wrap(err, 0)
	}
	if err := fs.WalkDir(modelFS, "maps", func(filename string, entry fs.DirEntry, err error) error {
		var smt serialMapType
		var mapType MapType
		if err = ldr.walkDir(filename, entry, err, &smt, &mapType.Name, &mapType); err != nil {
			return filterWalkDirError(err)
		}
		smt.asType(&ldr, &mapType)
		model.Maps = append(model.Maps, &mapType)
		return nil
	}); err != nil {
		return derrors.Wrap(err, 0)
	}
	if err := fs.WalkDir(modelFS, "structs", func(filename string, entry fs.DirEntry, err error) error {
		var sst serialStructType
		var structType StructType
		if err = ldr.walkDir(filename, entry, err, &sst, &structType.Name, &structType); err != nil {
			return filterWalkDirError(err)
		}
		sst.asType(&ldr, &structType)
		model.Structs = append(model.Structs, &structType)
		return nil
	}); err != nil {
		return derrors.Wrap(err, 0)
	}
	for name, rt := range ldr.resolve {
		if rt.declaration == nil {
			return derrors.Errorf("declaration missing for %s", name)
		}
		for _, usage := range rt.usages {
			*usage = rt.declaration
		}
	}
	for _, a := range model.Arrays {
		if a.GoType == nil {
			a.GoType = SliceOf(a.Element.GetGoType())
		}
	}
	for _, m := range model.Maps {
		if m.GoType == nil {
			m.GoType = MapOf(m.Key.GetGoType(), m.Value.GetGoType())
		}
	}
	return nil
}

func (l *loader) getResolveData(name string) *resolveData {
	if rd, ok := l.resolve[name]; ok {
		return rd
	}
	var rd resolveData
	l.resolve[name] = &rd
	return &rd
}

func (l *loader) walkDir(filename string, entry fs.DirEntry, ierr error, serializableType any, typName *string, typ Type) (err error) {
	err = ierr
	if err != nil {
		return derrors.Wrap(err, 0)
	}
	if entry.IsDir() {
		return isDir
	}
	if !strings.HasSuffix(filename, yamlSuffix) {
		return derrors.Errorf("expected .yaml found: %s", filename)
	}
	var file fs.File
	if file, err = l.modelFS.Open(filename); err != nil {
		return derrors.Wrap(err, 0)
	}
	defer dio.CloseHelper(file, &err)
	if err = yaml.NewDecoder(file).Decode(serializableType); err != nil {
		return derrors.Wrap(err, 0)
	}
	name := path.Base(filename)
	name = name[:len(name)-len(yamlSuffix)]
	*typName = name
	l.getResolveData(name).declaration = typ
	return nil
}

func filterWalkDirError(err error) error {
	if errors.Is(err, isDir) {
		return nil
	}
	if errors.Is(err, fs.ErrNotExist) {
		return nil
	}
	return err
}

func (l *loader) addUsage(name string, typ *Type) { //nolint:gocritic // intentionally a pointer to an interface
	rd := l.getResolveData(name)
	rd.usages = append(rd.usages, typ)
}

func (sb *serialBasic) asType(basic *Basic) {
	if sb.GoType != nil {
		basic.GoType = sb.GoType.buildGoType()
	}
	basic.PostgresType = sb.PostgresType
	basic.PostgresSerialType = sb.PostgresSerialType
	basic.Annotations = sb.Annotations
}

func (b *Basic) GetAnnotations() []string {
	return b.Annotations
}

func (b *Basic) GetGoType() GoType {
	return b.GoType
}
