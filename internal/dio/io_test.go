package dio_test

import (
	"context"
	"errors"
	"io"
	"testing"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dio"
	"github.com/stretchr/testify/require"
)

type testCloser struct {
	err error
}

var _ io.Closer = testCloser{}

func (tc testCloser) Close() error {
	return tc.err
}

func TestCloseHelper(t *testing.T) {
	t.Parallel()
	const origErr = derrors.StrError("origErr")
	const closeErr = derrors.StrError("closeErr")
	testCases := []struct {
		err         error
		closeErr    error
		expectedErr error
		name        string
	}{{}, {
		closeErr:    closeErr,
		expectedErr: closeErr,
	}, {
		err:         origErr,
		expectedErr: origErr,
	}, {
		err:         origErr,
		closeErr:    closeErr,
		expectedErr: origErr,
	}}
	for i := range testCases {
		testCase := &testCases[i]
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			err := testCase.err
			dio.CloseHelper(testCloser{err: testCase.closeErr}, &err)
			if testCase.expectedErr == nil {
				require.NoError(t, err)
			} else {
				require.True(t, errors.Is(err, testCase.expectedErr))
			}
		})
	}
}

type testCloserCtx struct {
	err error
}

var _ dio.CloserCtx = testCloserCtx{}

func (tcc testCloserCtx) Close(_ context.Context) error {
	return tcc.err
}

func TestCloseCtxHelper(t *testing.T) {
	t.Parallel()
	const origErr = derrors.StrError("origErr")
	const closeErr = derrors.StrError("closeErr")
	testCases := []struct {
		err         error
		closeErr    error
		expectedErr error
		name        string
	}{{}, {
		closeErr:    closeErr,
		expectedErr: closeErr,
	}, {
		err:         origErr,
		expectedErr: origErr,
	}, {
		err:         origErr,
		closeErr:    closeErr,
		expectedErr: origErr,
	}}
	for i := range testCases {
		testCase := &testCases[i]
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			err := testCase.err
			dio.CloseCtxHelper(context.Background(), testCloserCtx{err: testCase.closeErr}, &err)
			if testCase.expectedErr == nil {
				require.NoError(t, err)
			} else {
				require.True(t, errors.Is(err, testCase.expectedErr))
			}
		})
	}
}
