package dio

import (
	"context"
	"io"

	"codeberg.org/arichard/dcrud/internal/derrors"
)

func CloseHelper(f io.Closer, err *error) { //nolint:gocritic // intentionally a pointer to an interface
	if cerr := f.Close(); cerr != nil && *err == nil {
		*err = derrors.Wrap(cerr, 0)
	}
}

func DiscardCloseHelper(f io.ReadCloser, err *error) { //nolint:gocritic // intentionally a pointer to an interface
	if _, derr := io.Copy(io.Discard, f); derr != nil && *err == nil {
		*err = derrors.Wrap(derr, 0)
	}
	if cerr := f.Close(); cerr != nil && *err == nil {
		*err = derrors.Wrap(cerr, 0)
	}
}

type CloserCtx interface {
	Close(context.Context) error
}

func CloseCtxHelper(ctx context.Context, cc CloserCtx, err *error) { //nolint:gocritic // intentionally a pointer to an interface
	if cerr := cc.Close(ctx); cerr != nil && *err == nil {
		*err = derrors.Wrap(cerr, 0)
	}
}
