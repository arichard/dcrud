package dhttp_test

import (
	"bytes"
	"fmt"
	"net/http"
	"testing"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dhttp"
	"github.com/stretchr/testify/require"
)

type testLogger struct {
	buf bytes.Buffer
}

func (l *testLogger) Print(args ...any) {
	fmt.Fprint(&l.buf, args...)
}

func (l *testLogger) Printf(format string, args ...any) {
	fmt.Fprintf(&l.buf, format, args...)
}

type testResponseWriter struct {
	header     http.Header
	buf        bytes.Buffer
	statusCode int
}

func (resp *testResponseWriter) Header() http.Header {
	return resp.header
}

func (resp *testResponseWriter) Write(b []byte) (int, error) {
	return resp.buf.Write(b) //nolint:wrapcheck // Not needed for test
}

func (resp *testResponseWriter) WriteHeader(statusCode int) {
	resp.statusCode = statusCode
}

func TestWriteJSON(t *testing.T) {
	t.Parallel()
	logger := testLogger{}
	resp := testResponseWriter{
		header: make(http.Header),
	}
	dhttp.WriteResp(&logger, func(_ *http.Request) (int, any, error) {
		return http.StatusTeapot, struct {
			Hello string `json:"hello"`
		}{
			Hello: "world",
		}, nil
	}, &resp, nil)
	require.Empty(t, logger.buf.String())
	require.Equal(t, http.StatusTeapot, resp.statusCode)
	require.Contains(t, resp.header, "Content-Type")
	require.Equal(t, `{"hello":"world"}`, string(bytes.TrimSpace(resp.buf.Bytes())))
}

func TestWriteErr(t *testing.T) {
	t.Parallel()
	const message = "something fairly unique"
	testCases := []struct {
		err  error
		name string
	}{{
		err: derrors.StrError(message),
	}, {
		err: derrors.New(message),
	}, {
		err: derrors.Errorf("%s", message),
	}, {
		err: derrors.Wrap(derrors.StrError(message), 0),
	}}
	for i := range testCases {
		testCase := &testCases[i]
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			logger := testLogger{}
			resp := testResponseWriter{
				header: make(http.Header),
			}
			dhttp.WriteResp(&logger, func(_ *http.Request) (int, any, error) {
				return http.StatusInternalServerError, nil, testCase.err
			}, &resp, nil)
			require.Contains(t, logger.buf.String(), message)
			require.Equal(t, http.StatusInternalServerError, resp.statusCode)
			require.Contains(t, resp.header, "Content-Type")
			require.Contains(t, resp.buf.String(), message)
		})
	}
}
