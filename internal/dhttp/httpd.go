package dhttp

import (
	"encoding/json"
	"errors"
	"net/http"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dlog"
)

func WriteResp(logger dlog.Logger, handler func(*http.Request) (int, any, error), resp http.ResponseWriter, req *http.Request) {
	statusCode, data, err := handler(req)
	if err != nil {
		derrors.LogErr(logger, err)
		var dhe *derrors.HTTPError
		if errors.As(err, &dhe) {
			statusCode = dhe.HTTPStatus()
		}
		data = struct {
			JSON  error  `json:"json"`
			Error string `json:"error"`
		}{
			JSON:  err,
			Error: err.Error(),
		}
	}
	resp.WriteHeader(statusCode)
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	if err = json.NewEncoder(resp).Encode(data); err != nil {
		derrors.LogErr(logger, err)
	}
}
