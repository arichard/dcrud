package dhttp_test

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"testing"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dhttp"
	"github.com/stretchr/testify/require"
)

type testRoundTripper struct {
	roundTrip func(*http.Request) (*http.Response, error)
}

func (rt *testRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	return rt.roundTrip(req)
}

func TestHTTPSimple(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		headers    http.Header
		name       string
		method     string
		url        string
		respBody   string
		error      string
		errBody    string
		statusCode int
	}{{
		method:     http.MethodGet,
		url:        "https://localhost:8443/test",
		statusCode: http.StatusOK,
		respBody:   "hello world",
	}, {
		method:     http.MethodGet,
		url:        "https://localhost:8443/foo",
		headers:    http.Header{"Authorization": []string{"Bearer blah"}},
		statusCode: http.StatusOK,
		respBody:   "permitted",
	}, {
		method:     http.MethodPost,
		url:        "https://localhost:8443/baz",
		statusCode: http.StatusInternalServerError,
		errBody:    "server oops",
	}, {
		method: http.MethodGet,
		url:    "https://localhost:8443/bar",
		error:  "client oops",
	}}
	for i := range testCases {
		testCase := &testCases[i]
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			var respBody string
			var errBody string
			respCode, err := dhttp.HTTPSimple(context.Background(), &http.Client{
				Transport: &testRoundTripper{
					roundTrip: func(req *http.Request) (*http.Response, error) {
						require.Equal(t, testCase.method, req.Method)
						require.Equal(t, testCase.url, req.URL.String())
						if testCase.headers == nil {
							require.Len(t, req.Header, 0)
						} else {
							require.Equal(t, testCase.headers, req.Header)
						}
						if testCase.error != "" {
							return nil, derrors.StrError(testCase.error)
						}
						return &http.Response{
							StatusCode: testCase.statusCode,
							Body:       io.NopCloser(bytes.NewBuffer([]byte(`"` + testCase.respBody + testCase.errBody + `"`))),
						}, nil
					},
				},
			}, testCase.method, testCase.url, testCase.headers, &respBody, &errBody)
			if testCase.error == "" {
				require.NoError(t, err)
			} else {
				require.Contains(t, err.Error(), testCase.error)
			}
			require.Equal(t, testCase.statusCode, respCode)
			require.Equal(t, testCase.respBody, respBody)
			require.Equal(t, testCase.errBody, errBody)
		})
	}
}

func TestHTTPBody(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		headers    http.Header
		name       string
		method     string
		url        string
		reqBody    string
		respBody   string
		error      string
		errBody    string
		statusCode int
	}{{
		method:     http.MethodPost,
		url:        "https://localhost:8443/upload",
		reqBody:    "request body",
		statusCode: http.StatusOK,
		respBody:   "response body",
	}, {
		method:     http.MethodPut,
		url:        "https://localhost:8443/something",
		headers:    http.Header{"Content-Type": []string{"override"}},
		reqBody:    "more data",
		statusCode: http.StatusOK,
		respBody:   "result",
	}}
	for i := range testCases {
		testCase := &testCases[i]
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			var buf bytes.Buffer
			var respBody string
			var errBody string
			respCode, err := dhttp.HTTPBody(context.Background(), &http.Client{
				Transport: &testRoundTripper{
					roundTrip: func(req *http.Request) (*http.Response, error) {
						require.Equal(t, testCase.method, req.Method)
						require.Equal(t, testCase.url, req.URL.String())
						if testCase.headers == nil {
							require.Len(t, req.Header, 1)
							require.Contains(t, req.Header.Get("Content-Type"), "application/json")
						} else {
							require.Equal(t, testCase.headers, req.Header)
						}
						body, err := io.ReadAll(req.Body)
						require.NoError(t, err)
						require.Equal(t, `"`+testCase.reqBody+`"`, string(bytes.TrimSpace(body)))
						if testCase.error != "" {
							return nil, derrors.StrError(testCase.error)
						}
						return &http.Response{
							StatusCode: testCase.statusCode,
							Body:       io.NopCloser(bytes.NewBuffer([]byte(`"` + testCase.respBody + testCase.errBody + `"`))),
						}, nil
					},
				},
			}, &buf, testCase.method, testCase.url, testCase.headers, testCase.reqBody, &respBody, &errBody)
			if testCase.error == "" {
				require.NoError(t, err)
			} else {
				require.Contains(t, err.Error(), testCase.error)
			}
			require.Equal(t, testCase.statusCode, respCode)
			require.Equal(t, testCase.respBody, respBody)
			require.Equal(t, testCase.errBody, errBody)
		})
	}
}
