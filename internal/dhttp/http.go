package dhttp

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dio"
)

func HTTPSimple(ctx context.Context, httpClient *http.Client, method, url string, header http.Header, respBody, errBody any) (respCode int, err error) {
	var req *http.Request
	if req, err = http.NewRequestWithContext(ctx, method, url, http.NoBody); err != nil {
		return 0, derrors.Wrap(err, 0)
	}
	if header != nil {
		req.Header = header
	}
	var resp *http.Response
	if resp, err = httpClient.Do(req); err != nil {
		return 0, derrors.Wrap(err, 0)
	}
	defer dio.DiscardCloseHelper(resp.Body, &err)
	var body any
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		body = respBody
	} else {
		body = errBody
	}
	if err = json.NewDecoder(resp.Body).Decode(body); err != nil {
		return resp.StatusCode, derrors.Wrap(err, 0)
	}
	return resp.StatusCode, nil
}

func HTTPBody(ctx context.Context, httpClient *http.Client, buf *bytes.Buffer, method, url string, header http.Header, reqBody, respBody, errBody any) (respCode int, err error) { //nolint:lll // Lots of arguments
	buf.Reset()
	if err = json.NewEncoder(buf).Encode(reqBody); err != nil {
		return 0, derrors.Wrap(err, 0)
	}
	var req *http.Request
	if req, err = http.NewRequestWithContext(ctx, method, url, buf); err != nil {
		return 0, derrors.Wrap(err, 0)
	}
	if header != nil {
		req.Header = header
	}
	if req.Header.Get("Content-Type") == "" {
		req.Header.Set("Content-Type", "application/json; charset=utf-8")
	}
	var resp *http.Response
	if resp, err = httpClient.Do(req); err != nil {
		return 0, derrors.Wrap(err, 0)
	}
	defer dio.DiscardCloseHelper(resp.Body, &err)
	var body any
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		body = respBody
	} else {
		body = errBody
	}
	if err = json.NewDecoder(resp.Body).Decode(body); err != nil {
		return resp.StatusCode, derrors.Wrap(err, 0)
	}
	return resp.StatusCode, nil
}
