package k8s

import (
	"bytes"
	"context"
	"encoding/json"

	"codeberg.org/arichard/dcrud/internal/derrors"
)

const AnnotateConflict = derrors.StrError("annotate conflict")

type Deployment struct {
	Metadata ObjectMeta `json:"metadata"`
}

type Pod struct {
	Metadata ObjectMeta `json:"metadata"`
}

type PodList struct {
	Items []*Pod `json:"items,omitempty"`
}

type ObjectMeta struct {
	Annotations     map[string]string `json:"annotations,omitempty"`
	Name            string            `json:"name,omitempty"`
	ResourceVersion string            `json:"resourceVersion,omitempty"` //nolint:tagliatelle // External API
}

type K8s interface {
	CurrentNamespace() string
	DeploymentAnnotate(ctx context.Context, buf *bytes.Buffer, namespace, deploymentName, resourceVersion, key string, value *string) (resourceVersionRet string, err error)
	DeploymentRead(ctx context.Context, namespace, podName string) (*Deployment, error)
	PodAnnotate(ctx context.Context, buf *bytes.Buffer, namespace, podName, resourceVersion, key string, value *string) (resourceVersionRet string, err error)
	PodList(ctx context.Context, buf *bytes.Buffer, namespace, labelSelector string) (*PodList, error)
	ConfigWatch(ctx context.Context, namespace, configMapName string, callback func(json.RawMessage)) (err error)
}
