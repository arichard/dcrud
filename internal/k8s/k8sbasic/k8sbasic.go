package k8sbasic

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"errors"
	"io"
	"net/http"
	"net/url"
	"os"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dhttp"
	"codeberg.org/arichard/dcrud/internal/dio"
	"codeberg.org/arichard/dcrud/internal/k8s"
)

const (
	apiServer      = "https://kubernetes.default.svc"
	serviceAccount = "/var/run/secrets/kubernetes.io/serviceaccount"
)

type k8sBasic struct {
	httpClient *http.Client
	auth       string
	namespace  string
}

var _ k8s.K8s = &k8sBasic{}

func New() (k8s.K8s, error) {
	var kube k8sBasic
	token, err := os.ReadFile(serviceAccount + "/token")
	if err != nil {
		return nil, derrors.Wrap(err, 0)
	}
	kube.auth = "Bearer " + string(token)
	var caCert []byte
	if caCert, err = os.ReadFile(serviceAccount + "/ca.crt"); err != nil {
		return nil, derrors.Wrap(err, 0)
	}
	rootCAs := x509.NewCertPool()
	for len(caCert) > 0 {
		var block *pem.Block
		if block, caCert = pem.Decode(caCert); block == nil {
			break
		}
		var cert *x509.Certificate
		if cert, err = x509.ParseCertificate(block.Bytes); err != nil {
			return nil, derrors.Wrap(err, 0)
		}
		rootCAs.AddCert(cert)
	}
	defaultTransport, ok := http.DefaultTransport.(*http.Transport) // DR Make the base transport configurable?
	if !ok {
		return nil, derrors.Errorf("unexpected http.DefaultTransport type: %T", http.DefaultTransport)
	}
	transport := defaultTransport.Clone()
	transport.TLSClientConfig = &tls.Config{
		MinVersion: tls.VersionTLS13,
		RootCAs:    rootCAs,
	}
	kube.httpClient = &http.Client{
		// Intentionally no timeout, to enable watch, use contexts
		Transport: transport,
	}
	var namespace []byte
	if namespace, err = os.ReadFile(serviceAccount + "/namespace"); err != nil {
		return nil, derrors.Wrap(err, 0)
	}
	kube.namespace = string(namespace)
	return &kube, nil
}

func (k *k8sBasic) CurrentNamespace() string {
	return k.namespace
}

func (k *k8sBasic) DeploymentAnnotate(ctx context.Context, buf *bytes.Buffer, namespace, deploymentName, resourceVersion, key string, value *string) (resourceVersionRet string, err error) { //nolint:lll // Lots of arguments
	return k.annotate(ctx, buf, apiServer+"/apis/apps/v1/namespaces/"+url.PathEscape(namespace)+"/deployments/"+url.PathEscape(deploymentName), resourceVersion, key, value)
}

func (k *k8sBasic) DeploymentRead(ctx context.Context, namespace, podName string) (*k8s.Deployment, error) {
	var resp k8s.Deployment
	var errResp json.RawMessage
	statusCode, err := dhttp.HTTPSimple(ctx, k.httpClient, http.MethodGet,
		apiServer+"/apis/apps/v1/namespaces/"+url.PathEscape(namespace)+"/deployments/"+url.PathEscape(podName),
		k.header(), &resp, &errResp)
	if err != nil {
		return nil, err
	}
	if statusCode != http.StatusOK {
		return nil, derrors.Errorf("expected 200 found %d: %s", statusCode, string(errResp))
	}
	return &resp, nil
}

func (k *k8sBasic) PodAnnotate(ctx context.Context, buf *bytes.Buffer, namespace, podName, resourceVersion, key string, value *string) (resourceVersionRet string, err error) { //nolint:lll // Lots of arguments
	return k.annotate(ctx, buf, apiServer+"/api/v1/namespaces/"+url.PathEscape(namespace)+"/pods/"+url.PathEscape(podName), resourceVersion, key, value)
}

func (k *k8sBasic) PodList(ctx context.Context, buf *bytes.Buffer, namespace, labelSelector string) (*k8s.PodList, error) {
	buf.Reset()
	buf.WriteString(apiServer)
	buf.WriteString("/api/v1/namespaces/")
	buf.WriteString(url.PathEscape(namespace))
	buf.WriteString("/pods")
	if labelSelector != "" {
		buf.WriteString("?labelSelector=")
		buf.WriteString(url.QueryEscape(labelSelector))
	}
	var resp k8s.PodList
	var errResp json.RawMessage
	statusCode, err := dhttp.HTTPSimple(ctx, k.httpClient, http.MethodGet, buf.String(), k.header(), &resp, &errResp)
	if err != nil {
		return nil, err
	}
	if statusCode != http.StatusOK {
		return nil, derrors.Errorf("expected 200 found %d: %s", statusCode, string(errResp))
	}
	return &resp, nil
}

func (k *k8sBasic) ConfigWatch(ctx context.Context, namespace, configMapName string, callback func(json.RawMessage)) (err error) {
	basePath := apiServer + "/api/v1/namespaces/" + url.PathEscape(namespace) + "/configmaps?watch=true&fieldSelector=metadata.name%3D" + url.QueryEscape(configMapName)
	var resourceVersion string
	for {
		var path string
		if resourceVersion == "" {
			path = basePath
		} else {
			path = basePath + "&resourceVersion=" + url.QueryEscape(resourceVersion)
		}
		var req *http.Request
		if req, err = http.NewRequestWithContext(ctx, http.MethodGet, path, http.NoBody); err != nil {
			return derrors.Wrap(err, 0)
		}
		req.Header.Set("Authorization", k.auth)
		var resp *http.Response
		if resp, err = k.httpClient.Do(req); err != nil {
			return derrors.Wrap(err, 0)
		}
		defer dio.DiscardCloseHelper(resp.Body, &err)
		if resp.StatusCode != http.StatusOK {
			var body []byte
			if body, err = io.ReadAll(resp.Body); err != nil {
				return derrors.Errorf("expected 200 found %d", resp.StatusCode)
			}
			return derrors.Errorf("expected 200 found %d: %s", resp.StatusCode, string(body))
		}
		decoder := json.NewDecoder(resp.Body)
		for {
			var body struct {
				Object struct {
					Metadata struct {
						ResourceVersion string `json:"resourceVersion,omitempty"` //nolint:tagliatelle // External API
					} `json:"metadata"`
					Data json.RawMessage `json:"data,omitempty"`
				} `json:"object,omitempty"`
			}
			if err = decoder.Decode(&body); err != nil {
				if errors.Is(err, io.EOF) {
					break
				}
				return derrors.Wrap(err, 0)
			}
			resourceVersion = body.Object.Metadata.ResourceVersion
			callback(body.Object.Data)
		}
	}
}

func (k *k8sBasic) annotate(ctx context.Context, buf *bytes.Buffer, path, resourceVersion, key string, value *string) (resourceVersionRet string, err error) {
	header := k.header()
	header.Set("Content-Type", "application/merge-patch+json")
	type reqMeta struct {
		Annotations     map[string]*string `json:"annotations"`
		ResourceVersion string             `json:"resourceVersion,omitempty"` //nolint:tagliatelle // External API
	}
	var resp struct {
		Metadata struct {
			ResourceVersion string `json:"resourceVersion,omitempty"` //nolint:tagliatelle // External API
		} `json:"metadata"`
	}
	var errResp json.RawMessage
	var statusCode int
	if statusCode, err = dhttp.HTTPBody(ctx, k.httpClient, buf, http.MethodPatch, path, header, &struct {
		Metadata reqMeta `json:"metadata"`
	}{
		Metadata: reqMeta{
			Annotations: map[string]*string{
				key: value,
			},
			ResourceVersion: resourceVersion,
		},
	}, &resp, &errResp); err != nil {
		return "", err
	}
	switch statusCode {
	case http.StatusOK:
		return resp.Metadata.ResourceVersion, nil
	case http.StatusConflict:
		return "", k8s.AnnotateConflict
	default:
		return "", derrors.Errorf("expected 200 found %d: %s", statusCode, string(errResp))
	}
}

func (k *k8sBasic) header() http.Header {
	header := make(http.Header)
	header.Set("Authorization", k.auth)
	return header
}
