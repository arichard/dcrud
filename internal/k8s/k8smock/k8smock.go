package k8smock

import (
	"bytes"
	"context"
	"encoding/json"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/k8s"
)

type K8sMock struct{}

var _ k8s.K8s = &K8sMock{}

func New() *K8sMock {
	return &K8sMock{}
}

func (k *K8sMock) CurrentNamespace() string {
	return "default"
}

func (k *K8sMock) DeploymentAnnotate(_ context.Context, _ *bytes.Buffer, _, _, resourceVersion, _ string, _ *string) (string, error) {
	// DR ...
	return resourceVersion, nil
}

func (k *K8sMock) DeploymentRead(_ context.Context, _, podName string) (*k8s.Deployment, error) {
	// DR ...
	return &k8s.Deployment{
		Metadata: k8s.ObjectMeta{
			Name: podName,
		},
	}, nil
}

func (k *K8sMock) PodAnnotate(_ context.Context, _ *bytes.Buffer, _, _, resourceVersion, _ string, _ *string) (string, error) {
	// DR ...
	return resourceVersion, nil
}

func (k *K8sMock) PodList(_ context.Context, _ *bytes.Buffer, _, _ string) (*k8s.PodList, error) {
	// DR ...
	return &k8s.PodList{
		Items: []*k8s.Pod{{
			Metadata: k8s.ObjectMeta{
				Name: "hostname",
			},
		}},
	}, nil
}

func (k *K8sMock) ConfigWatch(ctx context.Context, _, _ string, f func(json.RawMessage)) (err error) {
	f(json.RawMessage(`{}`))
	<-ctx.Done()
	return derrors.Wrap(ctx.Err(), 0)
}
