package dsql

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"strings"

	"codeberg.org/arichard/dcrud/internal/dconfig"
	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dio"
	"codeberg.org/arichard/dcrud/internal/dlog"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgtype/pgxtype"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

const (
	EmptyPatch       = derrors.StrError("empty patch")
	Impossible       = derrors.StrError("impossible")
	NotFound         = derrors.StrError("not found")
	defaultPGPortStr = "5432"
)

func BuildConnURI(pgCfg *dconfig.Postgres) (string, error) {
	connURI := url.URL{
		Scheme: "postgres",
	}
	if pgCfg.DBName != "" {
		connURI.Path = "/" + url.PathEscape(pgCfg.DBName)
	}
	if pgCfg.Username != "" || pgCfg.Password != "" {
		if pgCfg.Password == "" {
			connURI.User = url.User(pgCfg.Username)
		} else {
			connURI.User = url.UserPassword(pgCfg.Username, pgCfg.Password)
		}
	}
	var buf strings.Builder
	if len(pgCfg.Hosts) > 0 {
		type hostPort struct {
			host string
			port string
		}
		hostPorts := make([]hostPort, len(pgCfg.Hosts))
		for idx, host := range pgCfg.Hosts {
			var err error
			if hostPorts[idx].host, hostPorts[idx].port, err = net.SplitHostPort(host); err != nil {
				var nae *net.AddrError
				if errors.As(err, &nae) && nae.Err != "missing port in address" {
					return "", derrors.Wrap(err, 0)
				}
				hostPorts[idx].host = host
				hostPorts[idx].port = defaultPGPortStr
			}
		}
		buf.WriteString("host=")
		const queryEscapedComma = "%2C"
		for i, hp := range hostPorts {
			if i > 0 {
				buf.WriteString(queryEscapedComma)
			}
			buf.WriteString(url.QueryEscape(hp.host))
		}
		buf.WriteString("&port=")
		for i, hp := range hostPorts {
			if i > 0 {
				buf.WriteString(queryEscapedComma)
			}
			buf.WriteString(url.QueryEscape(hp.port))
		}
	}
	if pgCfg.Params != nil {
		for key, value := range pgCfg.Params {
			if buf.Len() > 0 {
				buf.WriteByte('&')
			}
			buf.WriteString(url.QueryEscape(key))
			buf.WriteByte('=')
			buf.WriteString(url.QueryEscape(value))
		}
	}
	connURI.RawQuery = buf.String()
	return connURI.String(), nil
}

type PgxLoggerWrapper struct {
	logger dlog.Logger
}

func NewPgxLoggerWrapper(logger dlog.Logger) PgxLoggerWrapper {
	return PgxLoggerWrapper{logger: logger}
}

func (plw PgxLoggerWrapper) Log(_ context.Context, level pgx.LogLevel, msg string, data map[string]any) {
	plw.logger.Printf("%d %s %v", level, msg, data)
}

func Connect(ctx context.Context, pgCfg *dconfig.Postgres, pgxLogger pgx.Logger) (*pgx.Conn, error) {
	connURI, err := BuildConnURI(pgCfg)
	if err != nil {
		return nil, err
	}
	var pgxCfg *pgx.ConnConfig
	if pgxCfg, err = pgx.ParseConfig(connURI); err != nil {
		return nil, derrors.Wrap(err, 0)
	}
	pgxCfg.Logger = pgxLogger
	var conn *pgx.Conn
	if conn, err = pgx.ConnectConfig(ctx, pgxCfg); err != nil {
		return nil, derrors.Wrap(err, 0)
	}
	return conn, nil
}

func ConnectPool(ctx context.Context, pgCfg *dconfig.Postgres, pgxLogger pgx.Logger) (*pgxpool.Pool, error) {
	connURI, err := BuildConnURI(pgCfg)
	if err != nil {
		return nil, err
	}
	var pgxPoolCfg *pgxpool.Config
	if pgxPoolCfg, err = pgxpool.ParseConfig(connURI); err != nil {
		return nil, derrors.Wrap(err, 0)
	}
	pgxCfg := pgxPoolCfg.ConnConfig
	pgxCfg.Logger = pgxLogger
	var conn *pgxpool.Pool
	if conn, err = pgxpool.ConnectConfig(ctx, pgxPoolCfg); err != nil {
		return nil, derrors.Wrap(err, 0)
	}
	return conn, nil
}

func connectDefault(ctx context.Context, pgCfg *dconfig.Postgres, pgxLogger pgx.Logger) (*pgx.Conn, error) {
	defaultPGCfg := *pgCfg
	defaultPGCfg.DBName = "postgres"
	// DR Do other parameters need to be changed?
	return Connect(ctx, &defaultPGCfg, pgxLogger)
}

func dbExists(ctx context.Context, conn *pgx.Conn, dbName string) (bool, error) {
	var count int
	if err := conn.QueryRow(ctx, `SELECT count(*) FROM "pg_database" WHERE "pg_database"."datname" = $1`, dbName).Scan(&count); err != nil {
		return false, derrors.Wrap(err, 0)
	}
	return count > 0, nil
}

func CreateDB(ctx context.Context, pgCfg *dconfig.Postgres, pgxLogger pgx.Logger) (err error) {
	var conn *pgx.Conn
	if conn, err = connectDefault(ctx, pgCfg, pgxLogger); err != nil {
		return err
	}
	defer dio.CloseCtxHelper(ctx, conn, &err)
	var exists bool
	if exists, err = dbExists(ctx, conn, pgCfg.DBName); err != nil {
		return err
	}
	if !exists {
		if _, err = conn.Exec(ctx, fmt.Sprintf(`CREATE DATABASE %q`, pgCfg.DBName)); err != nil {
			return derrors.Wrap(err, 0)
		}
	}
	return nil
}

func DropDB(ctx context.Context, pgCfg *dconfig.Postgres, pgxLogger pgx.Logger) (err error) {
	var conn *pgx.Conn
	if conn, err = connectDefault(ctx, pgCfg, pgxLogger); err != nil {
		return err
	}
	defer dio.CloseCtxHelper(ctx, conn, &err)
	var exists bool
	if exists, err = dbExists(ctx, conn, pgCfg.DBName); err != nil {
		return err
	}
	if exists {
		if _, err = conn.Exec(ctx, fmt.Sprintf(`DROP DATABASE %q`, pgCfg.DBName)); err != nil {
			return derrors.Wrap(err, 0)
		}
	}
	return nil
}

func QuerySingle(ctx context.Context, querier pgxtype.Querier, sql string, args, dest []any) error {
	row, err := querier.Query(ctx, sql, args...)
	if err != nil {
		return pgxError(err)
	}
	defer row.Close()
	if !row.Next() {
		if err = row.Err(); err != nil {
			return pgxError(err)
		}
		return derrors.HTTPErrorWrap(http.StatusNotFound, NotFound)
	}
	if err = row.Scan(dest...); err != nil {
		return pgxError(err)
	}
	if row.Next() {
		return derrors.Wrap(Impossible, 0)
	}
	if err = row.Err(); err != nil {
		return pgxError(err)
	}
	return nil
}

func ExecSingle(ctx context.Context, querier pgxtype.Querier, sql string, args ...any) error {
	cmdTag, err := querier.Exec(ctx, sql, args...)
	if err != nil {
		return pgxError(err)
	}
	rowsAffected := cmdTag.RowsAffected()
	if rowsAffected < 1 {
		return derrors.HTTPErrorWrap(http.StatusNotFound, NotFound)
	}
	if rowsAffected > 1 {
		return derrors.Wrap(Impossible, 0)
	}
	return nil
}

func PatchField(buf *bytes.Buffer, args []any, arg any, name string) []any {
	if len(args) > 0 {
		buf.WriteString(", ")
	}
	args = append(args, arg)
	fmt.Fprintf(buf, "%q = $%d\n", name, len(args))
	return args
}

func pgxError(err error) error {
	var ppe *pgconn.PgError
	as := errors.As(err, &ppe)
	err = derrors.Wrap(err, 1)
	if !as {
		return err
	}
	code := http.StatusInternalServerError
	switch ppe.Code {
	case "23502": // not_null_violation
		code = http.StatusBadRequest
	case "23505": // unique_violation
		code = http.StatusConflict
	}
	return derrors.HTTPErrorWrap(code, err)
}
