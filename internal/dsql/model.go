package dsql

import (
	"bytes"
	"context"
	"strings"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dname"
	"codeberg.org/arichard/dcrud/internal/metamodel"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func AddTables(ctx context.Context, conn *pgxpool.Pool, model *metamodel.Model, buf *bytes.Buffer) ([]*metamodel.StructType, error) {
	// Derived from `psql -c '\dt' -E`
	type table struct {
		name string
		id   uint32
	}
	var tables []*table
	var relname string
	var oid uint32
	if _, err := conn.QueryFunc(ctx, `SELECT pg_class.relname
, pg_class.oid
FROM pg_catalog.pg_class
LEFT JOIN pg_catalog.pg_namespace ON pg_namespace.oid = pg_class.relnamespace
WHERE pg_class.relkind IN ('r','p','')
AND pg_namespace.nspname != 'pg_catalog'
AND pg_namespace.nspname !~ '^pg_toast'
AND pg_namespace.nspname != 'information_schema'
AND pg_catalog.pg_table_is_visible(pg_class.oid)`, nil, []any{
		&relname,
		&oid,
	}, func(_ pgx.QueryFuncRow) error { // DR Use the dsql version of this
		tables = append(tables, &table{
			name: relname,
			id:   oid,
		})
		return nil
	}); err != nil {
		return nil, derrors.Wrap(err, 0)
	}

	// Derived from `psql -c '\d <table>' -E`
	structTypes := make([]*metamodel.StructType, len(tables))
	for idx, table := range tables {
		structType := metamodel.StructType{
			Basic: metamodel.Basic{
				Name: dname.ToSnakeCase(table.name, buf),
			},
		}
		var attname string
		var atttyp string
		if _, err := conn.QueryFunc(ctx, `SELECT pg_attribute.attname
, pg_catalog.format_type(pg_attribute.atttypid, pg_attribute.atttypmod)
FROM pg_catalog.pg_attribute
WHERE pg_attribute.attrelid = $1
AND pg_attribute.attnum > 0
AND NOT pg_attribute.attisdropped`, []any{table.id}, []any{
			&attname,
			&atttyp,
		}, func(_ pgx.QueryFuncRow) error { // DR Use the dsql version of this
			structType.Fields = append(structType.Fields, &metamodel.StructField{
				Name: dname.ToSnakeCase(attname, buf),
				Type: addType(model, atttyp, buf),
			})
			return nil
		}); err != nil {
			return nil, derrors.Wrap(err, 0)
		}
		// DR Verify this type doesn't already exist
		model.Structs = append(model.Structs, &structType)
		structTypes[idx] = &structType
	}

	return structTypes, nil
}

func addType(model *metamodel.Model, atttyp string, buf *bytes.Buffer) metamodel.Type {
	// Is this already defined as a type? Try the simplist type first.
	for _, pt := range model.Primitives {
		if pt.PostgresType == atttyp {
			return pt
		}
	}
	// Is this an array?
	for _, at := range model.Arrays {
		if at.GetPostgresType() == atttyp {
			return at
		}
	}
	// DR Check other types?
	if strings.HasSuffix(atttyp, "[]") {
		at := metamodel.ArrayType{
			Element: addType(model, atttyp[:len(atttyp)-2], buf),
		}
		model.Arrays = append(model.Arrays, &at)
		return &at
	}
	primitiveType := metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:         dname.ToSnakeCase(atttyp, buf),
			PostgresType: atttyp,
		},
	}
	model.Primitives = append(model.Primitives, &primitiveType)
	return &primitiveType
}
