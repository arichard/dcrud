package dsql_test

import (
	"bytes"
	"testing"

	"codeberg.org/arichard/dcrud/internal/dconfig"
	"codeberg.org/arichard/dcrud/internal/dsql"
	"github.com/stretchr/testify/require"
)

func TestBuildConnURI(t *testing.T) {
	t.Parallel()
	// Derived from https://www.postgresql.org/docs/14/libpq-connect.html
	testCases := []struct {
		name            string
		expectedConnURI string
		pgCfg           dconfig.Postgres
	}{{
		expectedConnURI: "postgres:",
	}, {
		expectedConnURI: "postgres:?host=localhost&port=5432",
		pgCfg: dconfig.Postgres{
			Hosts: []string{"localhost"},
		},
	}, {
		expectedConnURI: "postgres:?host=localhost&port=5433",
		pgCfg: dconfig.Postgres{
			Hosts: []string{"localhost:5433"},
		},
	}, {
		expectedConnURI: "postgres:///mydb?host=localhost&port=5432",
		pgCfg: dconfig.Postgres{
			Hosts:  []string{"localhost"},
			DBName: "mydb",
		},
	}, {
		expectedConnURI: "postgres://user@?host=localhost&port=5432",
		pgCfg: dconfig.Postgres{
			Username: "user",
			Hosts:    []string{"localhost"},
		},
	}, {
		expectedConnURI: "postgres://user:secret@?host=localhost&port=5432",
		pgCfg: dconfig.Postgres{
			Username: "user",
			Password: "secret",
			Hosts:    []string{"localhost"},
		},
	}, {
		expectedConnURI: "postgres://other@/otherdb?host=localhost&port=5432&connect_timeout=10",
		pgCfg: dconfig.Postgres{
			Username: "other",
			Hosts:    []string{"localhost"},
			DBName:   "otherdb",
			Params: map[string]string{
				"connect_timeout": "10",
				// "application_name": "myapp", // Only have one Param as the order is not stable
			},
		},
	}, {
		expectedConnURI: "postgres:///somedb?host=host1%2Chost2&port=123%2C456&target_session_attrs=any",
		pgCfg: dconfig.Postgres{
			Hosts: []string{
				"host1:123",
				"host2:456",
			},
			DBName: "somedb",
			Params: map[string]string{
				"target_session_attrs": "any",
				// "application_name": "myapp",  // Only have one Param as the order is not stable
			},
		},
	}}
	for i := range testCases {
		tc := &testCases[i]
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			connURI, err := dsql.BuildConnURI(&tc.pgCfg)
			require.NoError(t, err)
			require.Equal(t, tc.expectedConnURI, connURI)
		})
	}
}

func TestPatchField(t *testing.T) {
	t.Parallel()
	var buf bytes.Buffer
	var args []any

	args = dsql.PatchField(&buf, args, 1, "one")
	require.Equal(t, `"one" = $1
`, buf.String())
	require.Equal(t, []any{1}, args)

	args = dsql.PatchField(&buf, args, 2, "two")
	require.Equal(t, `"one" = $1
, "two" = $2
`, buf.String())
	require.Equal(t, []any{1, 2}, args)
}
