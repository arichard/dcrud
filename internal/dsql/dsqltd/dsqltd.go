package dsqltd

import (
	"context"
	"os"
	"strconv"
	"time"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dlog"
	"github.com/jackc/pgx/v4"
)

//nolint:gochecknoglobals // Don't care about globals in unit tests
var (
	PostgresFound bool
	PGUser        = "postgres"
)

func PGWait(logger dlog.Logger) error {
	// DR Changing DCRUD_POSTGRES_FOUND won't cause cached test results to be rerun, would test args be better?
	var err error
	if PostgresFound, err = strconv.ParseBool(os.Getenv("DCRUD_POSTGRES_FOUND")); err != nil {
		PostgresFound = false
	} else if PostgresFound {
		var postgresWait bool
		if postgresWait, err = strconv.ParseBool(os.Getenv("DCRUD_POSTGRES_WAIT")); err == nil && postgresWait {
			if err = pgWait(logger); err != nil {
				return err
			}
		}
	}
	return nil
}

func pgWait(logger dlog.Logger) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second) //nolint:gomnd // test code
	defer cancel()
	if e := os.Getenv("PGUSER"); e != "" {
		PGUser = e
	}
	pgxCfg, err := pgx.ParseConfig("postgres://" + PGUser + "@")
	if err != nil {
		return derrors.Wrap(err, 0)
	}
	for {
		var conn *pgx.Conn
		if conn, err = pgx.ConnectConfig(ctx, pgxCfg); err != nil {
			if ctx.Err() != nil {
				return derrors.Wrap(err, 0)
			}
			logger.Printf("postgres connect failed: %s", err.Error())
			time.Sleep(time.Second)
			continue
		}
		if err = conn.Close(ctx); err != nil {
			return derrors.Wrap(err, 0)
		}
		break
	}
	return nil
}
