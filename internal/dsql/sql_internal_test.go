package dsql

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"codeberg.org/arichard/dcrud/internal/dconfig"
	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dlog"
	"codeberg.org/arichard/dcrud/internal/dsql/dsqltd"
	"github.com/jackc/pgx/v4"
	"github.com/stretchr/testify/require"
)

//nolint:gochecknoglobals // Don't care about globals in unit tests
var (
	Logger    dlog.Logger
	PGXLogger pgx.Logger
)

func TestMain(m *testing.M) {
	var err error
	Logger = log.Default()
	if err = dsqltd.PGWait(Logger); err != nil {
		derrors.LogErr(Logger, err)
		os.Exit(1)
	}
	PGXLogger = NewPgxLoggerWrapper(Logger)
	os.Exit(m.Run())
}

func TestCreateDropDB(t *testing.T) {
	t.Parallel()
	if !dsqltd.PostgresFound {
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	pgCfg := dconfig.Postgres{
		Username: dsqltd.PGUser,
		DBName:   fmt.Sprintf("dcrud_test_cd_%d", time.Now().UTC().UnixNano()),
	}

	// Connect to the default database for verification
	conn, err := connectDefault(ctx, &pgCfg, PGXLogger)
	require.NoError(t, err)
	defer func() {
		require.NoError(t, conn.Close(ctx))
	}()

	// Verify the DB doesn't exist yet
	var exists bool
	exists, err = dbExists(ctx, conn, pgCfg.DBName)
	require.NoError(t, err)
	require.False(t, exists)

	// Create the DB
	require.NoError(t, CreateDB(ctx, &pgCfg, PGXLogger))
	exists, err = dbExists(ctx, conn, pgCfg.DBName)
	require.NoError(t, err)
	require.True(t, exists)

	// Create shouldn't fail if the DB already exists
	require.NoError(t, CreateDB(ctx, &pgCfg, PGXLogger))
	exists, err = dbExists(ctx, conn, pgCfg.DBName)
	require.NoError(t, err)
	require.True(t, exists)

	// Drop the DB
	require.NoError(t, DropDB(ctx, &pgCfg, PGXLogger))
	exists, err = dbExists(ctx, conn, pgCfg.DBName)
	require.NoError(t, err)
	require.False(t, exists)

	// Drop shouldn't fail if the DB doesn't exist
	require.NoError(t, DropDB(ctx, &pgCfg, PGXLogger))
	exists, err = dbExists(ctx, conn, pgCfg.DBName)
	require.NoError(t, err)
	require.False(t, exists)
}
