package dsql_test

import (
	"bytes"
	"context"
	"fmt"
	"testing"
	"time"

	"codeberg.org/arichard/dcrud/internal/dconfig"
	"codeberg.org/arichard/dcrud/internal/dsql"
	"codeberg.org/arichard/dcrud/internal/dsql/dsqltd"
	"codeberg.org/arichard/dcrud/internal/metamodel"
	"codeberg.org/arichard/dcrud/internal/metamodel/mmtd"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/require"
)

func TestAddTables(t *testing.T) {
	t.Parallel()
	if !dsqltd.PostgresFound {
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	pgCfg := dconfig.Postgres{
		Username: dsqltd.PGUser,
		DBName:   fmt.Sprintf("dcrud_test_at_%d", time.Now().UTC().UnixNano()),
	}
	err := dsql.CreateDB(ctx, &pgCfg, dsql.PGXLogger)
	require.NoError(t, err)
	defer dsql.DropDB(ctx, &pgCfg, dsql.PGXLogger) //nolint:errcheck // try to cleanup, don't care if it fails
	var conn *pgxpool.Pool
	conn, err = dsql.ConnectPool(ctx, &pgCfg, dsql.PGXLogger)
	require.NoError(t, err)
	defer conn.Close()
	_, err = conn.Exec(ctx, `CREATE TABLE "foo" (
		"id" BIGSERIAL,
		"bar" INTEGER NOT NULL,
		"baz" BYTEA NOT NULL,
		"bip" TEXT[] NOT NULL
	)`)
	require.NoError(t, err)

	var buf bytes.Buffer
	testAddTables(ctx, t, conn, &buf)
	testAddTablesEmptyModel(ctx, t, conn, &buf)
}

func testAddTables(ctx context.Context, t *testing.T, conn *pgxpool.Pool, buf *bytes.Buffer) {
	model := metamodel.Model{
		Primitives: []*metamodel.PrimitiveType{
			&mmtd.Byte,
			&mmtd.Int32,
			&mmtd.Int64,
			&mmtd.String,
		},
		Arrays: []*metamodel.ArrayType{
			&mmtd.Strings,
			&mmtd.Bytes,
		},
	}
	structs, err := dsql.AddTables(ctx, conn, &model, buf)
	require.NoError(t, err)
	require.Len(t, structs, 1)
	require.Len(t, model.Primitives, 4)
	require.Len(t, model.Arrays, 2)
	require.Len(t, model.Maps, 0)
	require.Len(t, model.Structs, 1)
	require.Equal(t, &metamodel.StructType{
		Basic: metamodel.Basic{
			Name: "foo",
		},
		Fields: []*metamodel.StructField{{
			Name: "id",
			Type: &mmtd.Int64,
		}, {
			Name: "bar",
			Type: &mmtd.Int32,
		}, {
			Name: "baz",
			Type: &mmtd.Bytes,
		}, {
			Name: "bip",
			Type: &mmtd.Strings,
		}},
	}, structs[0])
	require.Equal(t, structs[0], model.Structs[0])
}

func testAddTablesEmptyModel(ctx context.Context, t *testing.T, conn *pgxpool.Pool, buf *bytes.Buffer) {
	var model metamodel.Model
	structs, err := dsql.AddTables(ctx, conn, &model, buf)
	require.NoError(t, err)
	require.Len(t, structs, 1)
	require.Len(t, model.Primitives, 4)
	require.Len(t, model.Arrays, 1)
	require.Len(t, model.Maps, 0)
	require.Len(t, model.Structs, 1)
	i32 := metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:         "integer",
			PostgresType: "integer",
		},
	}
	i64 := metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:         "bigint",
			PostgresType: "bigint",
		},
	}
	str := metamodel.PrimitiveType{
		Basic: metamodel.Basic{
			Name:         "text",
			PostgresType: "text",
		},
	}
	require.Equal(t, &metamodel.StructType{
		Basic: metamodel.Basic{
			Name: "foo",
		},
		Fields: []*metamodel.StructField{{
			Name: "id",
			Type: &i64,
		}, {
			Name: "bar",
			Type: &i32,
		}, {
			Name: "baz",
			// Code doesn't know go treats this as an array since the type name doesn't end with '[]'
			Type: &metamodel.PrimitiveType{
				Basic: metamodel.Basic{
					Name:         "bytea",
					PostgresType: "bytea",
				},
			},
		}, {
			Name: "bip",
			Type: &metamodel.ArrayType{
				Element: &str,
			},
		}},
	}, structs[0])
	require.Equal(t, structs[0], model.Structs[0])
}
