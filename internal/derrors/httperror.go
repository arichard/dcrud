package derrors

import (
	"errors"
	"fmt"
)

type HTTPStatusError interface {
	error
	HTTPStatus() int
}

type HTTPError struct {
	Err    error `json:"err"`
	Status int   `json:"status"`
}

var _ HTTPStatusError = &HTTPError{}

func HTTPErrorWrap(status int, err error) error {
	// Is this already wrapped?
	var hse HTTPStatusError
	if errors.As(err, &hse) {
		return err
	}
	return &HTTPError{
		Status: status,
		Err:    Wrap(err, 1),
	}
}

func (err *HTTPError) Error() string {
	return fmt.Sprintf("%d: %s", err.Status, err.Err.Error())
}

func (err *HTTPError) Unwrap() error {
	return err.Err
}

func (err *HTTPError) HTTPStatus() int {
	return err.Status
}
