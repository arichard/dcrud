package derrors

import (
	"fmt"
)

type StrError string

var _ error = StrError("")

const _ StrError = "is const"

func (se StrError) Error() string {
	return string(se)
}

func New(msg string) error {
	return Wrap(StrError(msg), 1)
}

func Errorf(format string, a ...any) error {
	return Wrap(StrError(fmt.Sprintf(format, a...)), 1)
}
