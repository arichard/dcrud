package derrors_test

import (
	"bytes"
	"errors"
	"fmt"
	"net/http"
	"testing"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dlog"
	"github.com/stretchr/testify/require"
)

type testLogger struct {
	bytes.Buffer
}

var _ dlog.Logger = &testLogger{}

func (tl *testLogger) Print(v ...any) {
	fmt.Fprint(tl, v...)
}

func (tl *testLogger) Printf(format string, v ...any) {
	fmt.Fprintf(tl, format, v...)
}

func TestLogError(t *testing.T) {
	t.Parallel()

	var logger testLogger
	const testMsg = "testMsg"
	const pattern = `^derrors\.StrError ` + testMsg + `
	\[codeberg\.org/arichard/dcrud/internal/derrors_test\.TestLogError\] errors_test\.go:[0-9]+
	\[`
	derrors.LogErr(&logger, derrors.StrError(testMsg))
	require.Regexp(t, `^derrors\.StrError `+testMsg, logger.String()) // The stack trace is messed up for some reason
	logger.Reset()
	derrors.LogErr(&logger, derrors.New(testMsg))
	require.Regexp(t, pattern, logger.String())
	logger.Reset()
	derrors.LogErr(&logger, derrors.Errorf("%s", testMsg))
	require.Regexp(t, pattern, logger.String())
	logger.Reset()
	derrors.LogErr(&logger, derrors.Wrap(derrors.StrError(testMsg), 0))
	require.Regexp(t, pattern, logger.String())
}

func TestDoubleWrap(t *testing.T) {
	t.Parallel()
	var err error
	err = derrors.StrError("first")
	err = derrors.Wrap(err, 0)
	err = derrors.HTTPErrorWrap(http.StatusOK, err)
	err = derrors.Wrap(err, 0)
	count := 0
	var dse derrors.StackError
	for ; errors.As(err, &dse); err = errors.Unwrap(dse) {
		count++
	}
	require.Equal(t, 1, count)
}
