package derrors

import (
	"bytes"
	"errors"
	"fmt"
	"path/filepath"

	"codeberg.org/arichard/dcrud/internal/dlog"
	serrors "github.com/go-errors/errors" //nolint:depguard // The only place it's permitted
)

type StackError interface {
	error
	StackFrames() []serrors.StackFrame
	TypeName() string
}

func LogErr(logger dlog.Logger, err error) {
	var dse StackError
	if !errors.As(err, &dse) {
		dse = serrors.Wrap(err, 1)
	}
	var buf bytes.Buffer
	buf.WriteString(dse.TypeName())
	buf.WriteRune(' ')
	buf.WriteString(dse.Error())
	buf.WriteRune('\n')
	for _, frame := range dse.StackFrames() {
		buf.WriteString(fmt.Sprintf("\t[%s.%s] %s:%d\n", frame.Package, frame.Name, filepath.Base(frame.File), frame.LineNumber))
	}
	logger.Print(buf.String())
}

func Wrap(err error, skip int) error {
	// Is this already wrapped?
	var dse StackError
	if errors.As(err, &dse) {
		return err
	}
	return serrors.Wrap(err, skip+1)
}
