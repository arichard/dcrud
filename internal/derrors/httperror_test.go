package derrors_test

import (
	"errors"
	"testing"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"github.com/stretchr/testify/require"
)

func TestHTTPError(t *testing.T) {
	t.Parallel()
	const wrapped = derrors.StrError("wrapped")
	err := derrors.HTTPErrorWrap(401, wrapped)
	require.Equal(t, "401: wrapped", err.Error())
	require.True(t, errors.Is(err, wrapped))
	// Ensure a redundant wrap is ignored even if the next item in the chain isn't an HTTPError
	err = derrors.HTTPErrorWrap(404, derrors.Wrap(err, 0))
	require.Equal(t, "401: wrapped", err.Error())
}
