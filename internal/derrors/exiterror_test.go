package derrors_test

import (
	"errors"
	"testing"

	"codeberg.org/arichard/dcrud/internal/derrors"
	"github.com/stretchr/testify/require"
)

func TestExitError(t *testing.T) {
	t.Parallel()
	const wrapped = derrors.StrError("wrapped")
	err := derrors.ExitErrorWrap(2, wrapped)
	require.Equal(t, "2: wrapped", err.Error())
	require.True(t, errors.Is(err, wrapped))
	// Ensure a redundant wrap is ignored even if the next item in the chain isn't an ExitError
	err = derrors.ExitErrorWrap(3, derrors.Wrap(err, 0))
	require.Equal(t, "2: wrapped", err.Error())
}
