package derrors

import (
	"errors"
	"fmt"
)

type ExitCodeError interface {
	error
	ExitCode() int
}

type ExitError struct {
	Err  error `json:"err"`
	Code int   `json:"code"`
}

var _ ExitCodeError = &ExitError{}

func ExitErrorWrap(code int, err error) error {
	// Is this already wrapped?
	var ece ExitCodeError
	if errors.As(err, &ece) {
		return err
	}
	return &ExitError{
		Code: code,
		Err:  Wrap(err, 1),
	}
}

func (err *ExitError) Error() string {
	return fmt.Sprintf("%d: %s", err.Code, err.Err.Error())
}

func (err *ExitError) Unwrap() error {
	return err.Err
}

func (err *ExitError) ExitCode() int {
	return err.Code
}
