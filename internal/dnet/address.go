package dnet

import (
	"net"

	"codeberg.org/arichard/dcrud/internal/derrors"
)

const defaultAddress string = "127.0.0.1"

func FindLocalAddr() (string, error) {
	interfaces, err := net.Interfaces()
	if err != nil {
		return defaultAddress, derrors.Wrap(err, 0)
	}
	var fallback net.IP
	for _, iface := range interfaces {
		if (iface.Flags&net.FlagUp == 0) || (iface.Flags&net.FlagLoopback != 0) {
			continue
		}
		var addrs []net.Addr
		if addrs, err = iface.Addrs(); err != nil {
			// Hopefully unlikely so return an error
			return defaultAddress, derrors.Wrap(err, 0)
		}
		for _, addr := range addrs {
			ipNet, ok := addr.(*net.IPNet)
			if !ok {
				// Hopefully unlikely so return an error
				return defaultAddress, derrors.Errorf("unrecognized address type: %T", addr)
			}
			ipAddr := ipNet.IP
			if !ipAddr.IsGlobalUnicast() {
				continue
			}
			// Prefer IPv4
			if v4 := ipAddr.To4(); v4 != nil {
				return v4.String(), nil
			}
			if len(fallback) > 0 {
				continue
			}
			fallback = ipAddr
		}
	}
	if len(fallback) > 0 {
		return fallback.String(), nil
	}
	return defaultAddress, nil
}
