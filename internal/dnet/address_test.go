package dnet_test

import (
	"testing"

	"codeberg.org/arichard/dcrud/internal/dnet"
	"github.com/stretchr/testify/require"
)

func TestFindLocalAddr(t *testing.T) {
	t.Parallel()
	// Can't do too much but it shouldn't fail
	_, err := dnet.FindLocalAddr()
	require.NoError(t, err)
}
