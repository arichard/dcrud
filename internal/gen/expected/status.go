package expected

import (
	"errors"
	"net"
	"net/http"
	"time"

	"codeberg.org/arichard/dcrud/internal/dsql"
)

type statusResponse struct {
	Vcs           *vcsInfo `json:"vcs,omitempty"`
	Error         string   `json:"error,omitempty"`
	LocalAddress  string   `json:"local_address,omitempty"`
	RemoteAddress string   `json:"remote_address,omitempty"`
	Start         string   `json:"start"`
}

type vcsInfo struct {
	Modified string `json:"modified,omitempty"`
	Revision string `json:"revision,omitempty"`
	Time     string `json:"time,omitempty"`
}

func (s *Server) statusHandler(req *http.Request) (statusCode int, resp any, err error) {
	var remoteAddress string
	if remoteAddress, _, err = net.SplitHostPort(req.RemoteAddr); err != nil {
		remoteAddress = ""
	}
	httpStatus := http.StatusOK
	rsp := statusResponse{
		Vcs:           s.vcsInfo,
		Start:         s.start.Format(time.RFC3339),
		LocalAddress:  s.localAddr,
		RemoteAddress: remoteAddress,
	}
	ctx := req.Context()
	var fooBopID int64
	if err = dsql.QuerySingle(ctx, s.conn, `SELECT "foo_bop"."id" FROM "foo_bop" LIMIT 1`, nil, []any{&fooBopID}); err != nil && !errors.Is(err, dsql.NotFound) {
		httpStatus = http.StatusInternalServerError
		rsp.Error = err.Error()
	}
	return httpStatus, &rsp, nil
}
