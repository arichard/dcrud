// This code has been generated by httpd.tmpl, please don't edit, but keep the linters happy
package expected

import (
	"encoding/json"
	"net/http"
	"strings"

	"codeberg.org/arichard/dcrud/internal/derrors"
)

const (
	FooBopBase     = "/foo_bop"
	FooBopDir      = FooBopBase + "/"
	FooBopByBarDir = FooBopDir + "bar/"
)

type FooBopCreateResponse struct {
	ID int64 `json:"id"`
}

type FooBopUpdateResponse struct{}

type FooBopPatchResponse struct{}

type FooBopDeleteResponse struct{}

func (s *Server) fooBopCreateHandler(req *http.Request) (statusCode int, resp any, err error) {
	var fooBop FooBopCreatable
	if err = json.NewDecoder(req.Body).Decode(&fooBop); err != nil {
		return http.StatusBadRequest, nil, derrors.Wrap(err, 0)
	}
	ctx := req.Context()
	var fooBopID int64
	if fooBopID, err = FooBopCreate(ctx, s.conn, &fooBop); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	return http.StatusCreated, &FooBopCreateResponse{
		ID: fooBopID,
	}, nil
}

func (s *Server) fooBopReadHandler(req *http.Request) (statusCode int, resp any, err error) {
	var fooBopID int64
	if fooBopID, err = fooBopParsePath(req.URL.Path); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	ctx := req.Context()
	var fooBop *FooBop
	if fooBop, err = FooBopRead(ctx, s.conn, fooBopID); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	return http.StatusOK, fooBop, nil
}

func (s *Server) fooBopReadByBarHandler(req *http.Request) (statusCode int, resp any, err error) {
	var fooBopBar int32
	if fooBopBar, err = int32Parse(strings.TrimPrefix(req.URL.Path, FooBopByBarDir)); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	ctx := req.Context()
	var fooBop *FooBop
	if fooBop, err = FooBopReadByBar(ctx, s.conn, fooBopBar); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	return http.StatusOK, fooBop, nil
}

func (s *Server) fooBopUpdateHandler(req *http.Request) (statusCode int, resp any, err error) {
	var fooBopID int64
	if fooBopID, err = fooBopParsePath(req.URL.Path); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	var fooBop *FooBopUpdatable
	if err = json.NewDecoder(req.Body).Decode(&fooBop); err != nil {
		return http.StatusBadRequest, nil, derrors.Wrap(err, 0)
	}
	ctx := req.Context()
	if err = FooBopUpdate(ctx, s.conn, fooBopID, fooBop); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	return http.StatusOK, &FooBopUpdateResponse{}, nil
}

func (s *Server) fooBopPatchHandler(req *http.Request) (statusCode int, resp any, err error) {
	var fooBopID int64
	if fooBopID, err = fooBopParsePath(req.URL.Path); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	var fooBop *FooBopPatchable
	if err = json.NewDecoder(req.Body).Decode(&fooBop); err != nil {
		return http.StatusBadRequest, nil, derrors.Wrap(err, 0)
	}
	ctx := req.Context()
	if err = FooBopPatch(ctx, s.conn, fooBopID, fooBop); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	return http.StatusOK, &FooBopPatchResponse{}, nil
}

func (s *Server) fooBopDeleteHandler(req *http.Request) (statusCode int, resp any, err error) {
	var fooBopID int64
	if fooBopID, err = fooBopParsePath(req.URL.Path); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	ctx := req.Context()
	if err = FooBopDelete(ctx, s.conn, fooBopID); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	return http.StatusOK, &FooBopDeleteResponse{}, nil
}

func fooBopParsePath(path string) (int64, error) {
	return int64Parse(strings.TrimPrefix(path, FooBopDir))
}
