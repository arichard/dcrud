// This code has been generated by http.tmpl, please don't edit, but keep the linters happy
package expected

import (
	"bytes"
	"context"
	"net/http"

	"codeberg.org/arichard/dcrud/internal/dhttp"
)

type FooBopClient struct {
	httpClient *http.Client
	baseURL    string
}

func NewFooBopClient(baseURL string, httpClient *http.Client) *FooBopClient {
	return &FooBopClient{
		baseURL:    baseURL,
		httpClient: httpClient,
	}
}

func (fbc *FooBopClient) Create(ctx context.Context, buf *bytes.Buffer, fooBop *FooBopCreatable) (int64, error) {
	var resp FooBopCreateResponse
	if _, err := dhttp.HTTPBody(ctx, fbc.httpClient, buf, http.MethodPost, fbc.baseURL+FooBopBase, nil, fooBop, &resp, nil); err != nil {
		return 0, err
	}
	return resp.ID, nil
}

func (fbc *FooBopClient) Read(ctx context.Context, fooBopID int64) (*FooBop, error) {
	var fooBop FooBop
	if _, err := dhttp.HTTPSimple(ctx, fbc.httpClient, http.MethodGet, fooBopPath(fbc.baseURL, fooBopID), nil, &fooBop, nil); err != nil {
		return nil, err
	}
	return &fooBop, nil
}

func (fbc *FooBopClient) ReadByBar(ctx context.Context, fooBopBar int32) (*FooBop, error) {
	var fooBop FooBop
	if _, err := dhttp.HTTPSimple(ctx, fbc.httpClient, http.MethodGet, fbc.baseURL+FooBopByBarDir+int32Encode(fooBopBar), nil, &fooBop, nil); err != nil {
		return nil, err
	}
	return &fooBop, nil
}

func (fbc *FooBopClient) Update(ctx context.Context, buf *bytes.Buffer, fooBopID int64, fooBop *FooBopUpdatable) error {
	var resp FooBopUpdateResponse
	if _, err := dhttp.HTTPBody(ctx, fbc.httpClient, buf, http.MethodPut, fooBopPath(fbc.baseURL, fooBopID), nil, fooBop, &resp, nil); err != nil {
		return err
	}
	return nil
}

func (fbc *FooBopClient) Patch(ctx context.Context, buf *bytes.Buffer, fooBopID int64, fooBop *FooBopPatchable) error {
	var resp FooBopPatchResponse
	if _, err := dhttp.HTTPBody(ctx, fbc.httpClient, buf, http.MethodPatch, fooBopPath(fbc.baseURL, fooBopID), nil, fooBop, &resp, nil); err != nil {
		return err
	}
	return nil
}

func (fbc *FooBopClient) Delete(ctx context.Context, fooBopID int64) error {
	var resp FooBopDeleteResponse
	if _, err := dhttp.HTTPSimple(ctx, fbc.httpClient, http.MethodDelete, fooBopPath(fbc.baseURL, fooBopID), nil, &resp, nil); err != nil {
		return err
	}
	return nil
}

func fooBopPath(baseURL string, fooBopID int64) string {
	return baseURL + FooBopDir + int64Encode(fooBopID)
}
