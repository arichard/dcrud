package expected

import (
	"context"
	"errors"
	"log"
	"net"
	"net/http"
	"runtime/debug"
	"strconv"
	"strings"
	"time"

	"codeberg.org/arichard/dcrud/internal/cluster"
	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dhttp"
	"codeberg.org/arichard/dcrud/internal/dlog"
	"codeberg.org/arichard/dcrud/internal/dnet"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Server struct {
	start     time.Time
	cluster   cluster.Cluster
	logger    dlog.Logger
	conn      *pgxpool.Pool
	vcsInfo   *vcsInfo
	localAddr string
	httpd     http.Server
}

func NewServer(logger dlog.Logger, conn *pgxpool.Pool, clstr cluster.Cluster, fatalChan chan<- error) (*Server, error) {
	srvr := Server{
		start:  time.Now().UTC(),
		logger: logger,
		conn:   conn,
		httpd: http.Server{
			Addr:              ":8080", // DR Make configurable
			ReadTimeout:       time.Second,
			ReadHeaderTimeout: time.Second,
			WriteTimeout:      time.Second,
			IdleTimeout:       90 * time.Second, //nolint:gomnd // DR Make configurable
		},
		cluster: clstr,
	}
	srvr.httpd.Handler = &srvr
	if ll, ok := logger.(*log.Logger); ok {
		srvr.httpd.ErrorLog = ll
	} else {
		srvr.httpd.ErrorLog = log.New(logLoggerWrapper{logger: logger}, "", 0)
	}
	if buildInfo, ok := debug.ReadBuildInfo(); ok {
		var vcs vcsInfo
		for _, setting := range buildInfo.Settings {
			switch setting.Key {
			case "vcs.modified":
				vcs.Modified = setting.Value
			case "vcs.revision":
				vcs.Revision = setting.Value
			case "vcs.time":
				vcs.Time = setting.Value
			}
		}
		if vcs != (vcsInfo{}) {
			srvr.vcsInfo = &vcs
		}
	}
	var err error
	if srvr.localAddr, err = dnet.FindLocalAddr(); err != nil {
		derrors.LogErr(logger, err)
	}
	var l net.Listener
	if l, err = net.Listen("tcp", srvr.httpd.Addr); err != nil {
		return nil, derrors.Wrap(err, 0)
	}
	go func() {
		if err = srvr.httpd.Serve(l); err != nil && !errors.Is(err, http.ErrServerClosed) {
			fatalChan <- derrors.Wrap(err, 0)
		}
	}()
	return &srvr, nil
}

type logLoggerWrapper struct {
	logger dlog.Logger
}

func (logger logLoggerWrapper) Write(p []byte) (n int, err error) {
	logger.logger.Print(string(p))
	return len(p), nil
}

func (s *Server) Close(ctx context.Context) error {
	err := s.httpd.Shutdown(ctx)
	if err != nil {
		return derrors.Wrap(err, 0)
	}
	if err = s.httpd.Close(); err != nil {
		return derrors.Wrap(err, 0)
	}
	return nil
}

func (s *Server) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	ctxDuration := s.httpd.ReadTimeout - 100*time.Millisecond //nolint:gomnd // subtract a hopefully-reasonable amount
	altCtxDuration := 9 * s.httpd.ReadTimeout / 10            //nolint:gomnd // 90% of the original value
	if ctxDuration < altCtxDuration {
		ctxDuration = altCtxDuration
	}
	ctx, cancel := context.WithTimeout(req.Context(), ctxDuration)
	defer cancel()
	req = req.WithContext(ctx)
	switch req.Method {
	case http.MethodDelete:
		switch {
		case strings.HasPrefix(req.URL.Path, FooBopDir):
			dhttp.WriteResp(s.logger, s.fooBopDeleteHandler, resp, req)
		default:
			http.NotFound(resp, req)
		}
	case http.MethodGet:
		switch {
		case req.URL.Path == "/status":
			dhttp.WriteResp(s.logger, s.statusHandler, resp, req)
		case strings.HasPrefix(req.URL.Path, FooBopByBarDir):
			dhttp.WriteResp(s.logger, s.fooBopReadByBarHandler, resp, req)
		case strings.HasPrefix(req.URL.Path, FooBopDir):
			dhttp.WriteResp(s.logger, s.fooBopReadHandler, resp, req)
		default:
			http.NotFound(resp, req)
		}
	case http.MethodPatch:
		switch {
		case strings.HasPrefix(req.URL.Path, FooBopDir):
			dhttp.WriteResp(s.logger, s.fooBopPatchHandler, resp, req)
		default:
			http.NotFound(resp, req)
		}
	case http.MethodPost:
		switch {
		case req.URL.Path == FooBopBase:
			dhttp.WriteResp(s.logger, s.fooBopCreateHandler, resp, req)
		default:
			http.NotFound(resp, req)
		}
	case http.MethodPut:
		switch {
		case strings.HasPrefix(req.URL.Path, FooBopDir):
			dhttp.WriteResp(s.logger, s.fooBopUpdateHandler, resp, req)
		default:
			http.NotFound(resp, req)
		}
	default:
		http.NotFound(resp, req)
	}
}

func int32Encode(v int32) string {
	return strconv.FormatInt(int64(v), 10) //nolint:gomnd // Hopefully obvious magic numbers
}

func int32Parse(v string) (int32, error) {
	i, err := strconv.ParseInt(v, 10, 32) //nolint:gomnd // Hopefully obvious magic numbers
	if err != nil {
		return 0, derrors.Wrap(err, 0)
	}
	return int32(i), nil
}

func int64Encode(v int64) string {
	return strconv.FormatInt(v, 10) //nolint:gomnd // Hopefully obvious magic numbers
}

func int64Parse(v string) (int64, error) {
	i, err := strconv.ParseInt(v, 10, 64) //nolint:gomnd // Hopefully obvious magic numbers
	if err != nil {
		return 0, derrors.Wrap(err, 0)
	}
	return i, nil
}
