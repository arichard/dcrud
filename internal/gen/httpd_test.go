package gen_test

import (
	"bytes"
	"context"
	"net/http"
	"testing"
	"time"

	"codeberg.org/arichard/dcrud/internal/dio"
	"codeberg.org/arichard/dcrud/internal/dsql/dsqltd"
	"codeberg.org/arichard/dcrud/internal/gen/expected"
	"github.com/stretchr/testify/require"
)

func TestHttpd(t *testing.T) {
	t.Parallel()
	if !dsqltd.PostgresFound {
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	srvr, err := expected.NewServer(logger, conn, nil, nil)
	require.NoError(t, err)
	defer dio.CloseCtxHelper(ctx, srvr, &err)
	fbc := expected.NewFooBopClient("http://localhost:8080", &http.Client{
		Timeout: time.Second,
	})
	var buf bytes.Buffer

	fooBop := expected.FooBop{
		FooBopCreatable: expected.FooBopCreatable{
			FooBopUpdatable: expected.FooBopUpdatable{
				Bar:    123,
				Baz:    []byte{0xde, 0xad},
				BipBup: []string{"test", "httpd"},
			},
			Blah: true,
		},
	}
	fooBop.ID, err = fbc.Create(ctx, &buf, &fooBop.FooBopCreatable)
	require.NoError(t, err)

	var found *expected.FooBop
	found, err = fbc.Read(ctx, fooBop.ID)
	require.NoError(t, err)
	require.Equal(t, &fooBop, found)

	_, err = fbc.Read(ctx, -1)
	require.Error(t, err) // DR Verify this is a 'not found' error?

	found, err = fbc.ReadByBar(ctx, fooBop.Bar)
	require.NoError(t, err)
	require.Equal(t, &fooBop, found)

	_, err = fbc.ReadByBar(ctx, -1)
	require.Error(t, err) // DR Verify this is a 'not found' error?

	fooBop.Bar = 987
	fooBop.Baz = []byte{0xbe, 0xef}
	fooBop.BipBup = []string{"http", "post"}
	err = fbc.Update(ctx, &buf, fooBop.ID, &fooBop.FooBopUpdatable)
	require.NoError(t, err)
	found, err = fbc.Read(ctx, fooBop.ID)
	require.NoError(t, err)
	require.Equal(t, &fooBop, found)

	fooBop.Bar = 234
	fooBop.Baz = []byte{0xca, 0xfe}
	err = fbc.Patch(ctx, &buf, fooBop.ID, &expected.FooBopPatchable{
		Bar: &fooBop.Bar,
		Baz: &fooBop.Baz,
	})
	require.NoError(t, err)
	found, err = fbc.Read(ctx, fooBop.ID)
	require.NoError(t, err)
	require.Equal(t, &fooBop, found)

	fooBop.BipBup = []string{"http", "patch"}
	err = fbc.Patch(ctx, &buf, fooBop.ID, &expected.FooBopPatchable{
		BipBup: &fooBop.BipBup,
	})
	require.NoError(t, err)
	found, err = fbc.Read(ctx, fooBop.ID)
	require.NoError(t, err)
	require.Equal(t, &fooBop, found)

	err = fbc.Delete(ctx, fooBop.ID)
	require.NoError(t, err)
	_, err = fbc.Read(ctx, fooBop.ID)
	require.Error(t, err) // DR Verify this is a 'not found' error?
	err = fbc.Delete(ctx, fooBop.ID)
	require.Error(t, err) // DR Verify this is a 'not found' error?
}
