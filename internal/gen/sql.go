package gen

import (
	"bytes"
	"fmt"

	"codeberg.org/arichard/dcrud/internal/dname"
	"codeberg.org/arichard/dcrud/internal/metamodel"
)

func CreateTable(structType *metamodel.StructType, buf *bytes.Buffer) string {
	buf.Reset()
	buf.WriteString(`CREATE TABLE "`)
	dname.ToSnakeCaseAppend(structType.Name, buf)
	buf.WriteString(`" (`)
	for idx, field := range structType.Fields {
		if idx > 0 {
			buf.WriteString(", ")
		}
		var postgresType string
		if field.Serial {
			postgresType = field.Type.GetPostgresSerialType()
			// DR Warn if it doesn't make sense for this type to be serial? Warn earlier?
		} else {
			postgresType = field.Type.GetPostgresType()
		}
		buf.WriteByte('"')
		dname.ToSnakeCaseAppend(field.Name, buf)
		fmt.Fprintf(buf, `" %s`, postgresType)
		if field.NotNull {
			buf.WriteString(" NOT NULL")
		}
		buf.WriteByte('\n')
	}
	if len(structType.PrimaryKey) > 0 {
		buf.WriteString(", PRIMARY KEY(")
		writeFieldNames(structType.PrimaryKey, buf)
	}
	for _, unique := range structType.Uniques {
		buf.WriteString(", UNIQUE(")
		writeFieldNames(unique, buf)
	}
	buf.WriteString(")\n")
	return buf.String()
}

func writeFieldNames(fields []*metamodel.StructField, buf *bytes.Buffer) {
	for i, f := range fields {
		if i > 0 {
			buf.WriteString(", ")
		}
		buf.WriteByte('"')
		dname.ToSnakeCaseAppend(f.Name, buf)
		buf.WriteByte('"')
	}
	buf.WriteString(")\n")
}

func insertFields(structType *metamodel.StructType) (derived, fields []*metamodel.StructField) {
	for _, field := range structType.Fields {
		if creatable(field) {
			fields = append(fields, field)
		} else {
			derived = append(derived, field)
		}
	}
	return derived, fields
}

func InsertInto(structType *metamodel.StructType, buf *bytes.Buffer) string {
	name := dname.ToSnakeCase(structType.Name, buf)
	buf.Reset()
	fmt.Fprintf(buf, "INSERT INTO %q (", name)
	derived, fields := insertFields(structType)
	for i, field := range fields {
		if i > 0 {
			buf.WriteString(", ")
		}
		buf.WriteByte('"')
		dname.ToSnakeCaseAppend(field.Name, buf)
		buf.WriteString("\"\n")
	}
	buf.WriteString(")\nVALUES (")
	for i := range fields {
		if i > 0 {
			buf.WriteString(", ")
		}
		fmt.Fprintf(buf, "$%d\n", i+1)
	}
	buf.WriteByte(')')
	if len(derived) > 0 {
		buf.WriteString("\nRETURNING ")
	}
	writeTableFieldNames(name, derived, buf)
	return buf.String()
}

func writeTableFieldNames(name string, fields []*metamodel.StructField, buf *bytes.Buffer) {
	for i, f := range fields {
		if i > 0 {
			buf.WriteString(", ")
		}
		fmt.Fprintf(buf, `%q."`, name)
		dname.ToSnakeCaseAppend(f.Name, buf)
		buf.WriteString("\"\n")
	}
}

func selectFields(structType *metamodel.StructType) (primaryKey, fields []*metamodel.StructField) {
	for _, field := range structType.Fields {
		if !contains(structType.PrimaryKey, field) {
			fields = append(fields, field)
		}
	}
	return structType.PrimaryKey, fields
}

func Select(structType *metamodel.StructType, buf *bytes.Buffer) string {
	name := dname.ToSnakeCase(structType.Name, buf)
	buf.Reset()
	buf.WriteString("SELECT ")
	pks, fields := selectFields(structType)
	writeTableFieldNames(name, fields, buf)
	fmt.Fprintf(buf, "FROM %q\nWHERE ", name)
	writeTableFieldAssignNames(name, pks, 1, buf)
	buf.WriteString("LIMIT 1\n")
	return buf.String()
}

func writeTableFieldAssignNames(name string, fields []*metamodel.StructField, start int, buf *bytes.Buffer) {
	for idx, f := range fields {
		if idx > 0 {
			buf.WriteString(", ")
		}
		fmt.Fprintf(buf, `%q."`, name)
		dname.ToSnakeCaseAppend(f.Name, buf)
		fmt.Fprintf(buf, "\" = $%d\n", idx+start)
	}
}

func SelectBy(structType *metamodel.StructType, unique []*metamodel.StructField, buf *bytes.Buffer) string {
	name := dname.ToSnakeCase(structType.Name, buf)
	buf.Reset()
	buf.WriteString("SELECT ")
	first := true
	for _, field := range structType.Fields {
		if contains(unique, field) {
			continue
		}
		if first {
			first = false
		} else {
			buf.WriteString(", ")
		}
		fmt.Fprintf(buf, `%q."`, name)
		dname.ToSnakeCaseAppend(field.Name, buf)
		buf.WriteString("\"\n")
	}
	fmt.Fprintf(buf, "FROM %q\nWHERE ", name)
	writeTableFieldAssignNames(name, unique, 1, buf)
	buf.WriteString("LIMIT 1\n")
	return buf.String()
}

func updateFields(structType *metamodel.StructType) (primaryKey, fields []*metamodel.StructField) {
	for _, field := range structType.Fields {
		if updatable(field) {
			fields = append(fields, field)
		}
	}
	return structType.PrimaryKey, fields
}

func Update(structType *metamodel.StructType, buf *bytes.Buffer) string {
	name := dname.ToSnakeCase(structType.Name, buf)
	buf.Reset()
	pks, fields := updateFields(structType)
	fmt.Fprintf(buf, "UPDATE %q\nSET ", name)
	for idx, field := range fields {
		if idx > 0 {
			buf.WriteString(", ")
		}
		buf.WriteByte('"')
		dname.ToSnakeCaseAppend(field.Name, buf)
		fmt.Fprintf(buf, "\" = $%d\n", idx+1)
	}
	buf.WriteString("WHERE ")
	writeTableFieldAssignNames(name, pks, 1+len(fields), buf)
	return buf.String()
}

func deleteFields(st *metamodel.StructType) []*metamodel.StructField {
	return st.PrimaryKey
}

func DeleteFrom(st *metamodel.StructType, buf *bytes.Buffer) string {
	name := dname.ToSnakeCase(st.Name, buf)
	buf.Reset()
	pks := deleteFields(st)
	fmt.Fprintf(buf, "DELETE FROM %q\nWHERE ", name)
	writeTableFieldAssignNames(name, pks, 1, buf)
	return buf.String()
}
