package gen_test

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"codeberg.org/arichard/dcrud/internal/dconfig"
	"codeberg.org/arichard/dcrud/internal/derrors"
	"codeberg.org/arichard/dcrud/internal/dlog"
	"codeberg.org/arichard/dcrud/internal/dsql"
	"codeberg.org/arichard/dcrud/internal/dsql/dsqltd"
	"codeberg.org/arichard/dcrud/internal/gen/expected"
	"codeberg.org/arichard/dcrud/internal/metamodel"
	"codeberg.org/arichard/dcrud/internal/metamodel/mmtd"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/require"
)

//nolint:gochecknoglobals // Don't care about globals in unit tests
var (
	int64ID = metamodel.StructField{
		Name:   "ID",
		Type:   &mmtd.Int64,
		Serial: true,
	}
	fooBop    *metamodel.StructType
	audit     *metamodel.StructType
	logger    dlog.Logger
	pgxLogger pgx.Logger
	conn      *pgxpool.Pool
)

func TestMain(m *testing.M) {
	var err error
	logger = log.Default()
	if err = dsqltd.PGWait(logger); err != nil {
		derrors.LogErr(logger, err)
		os.Exit(1)
	}
	pgxLogger = dsql.NewPgxLoggerWrapper(logger)

	var model metamodel.Model
	if err = model.Load(metamodel.DefaultFS); err != nil {
		derrors.LogErr(logger, err)
		os.Exit(1)
	}
	if err = model.Load(expected.ModelFS); err != nil {
		derrors.LogErr(logger, err)
		os.Exit(1)
	}
	if fooBop, err = findStruct(model.Structs, "foo_bop"); err != nil {
		derrors.LogErr(logger, err)
		os.Exit(1)
	}
	if audit, err = findStruct(model.Structs, "audit"); err != nil {
		derrors.LogErr(logger, err)
		os.Exit(1)
	}

	if !dsqltd.PostgresFound {
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	pgCfg := dconfig.Postgres{
		Username: dsqltd.PGUser,
		DBName:   fmt.Sprintf("dcrud_test_crud_%d", time.Now().UTC().UnixNano()),
	}
	if err = dsql.CreateDB(ctx, &pgCfg, pgxLogger); err != nil {
		derrors.LogErr(logger, err)
		os.Exit(1)
	}
	if conn, err = dsql.ConnectPool(ctx, &pgCfg, pgxLogger); err != nil {
		derrors.LogErr(logger, err)
		os.Exit(1)
	}
	if err = expected.FooBopCreateTable(ctx, conn); err != nil {
		derrors.LogErr(logger, err)
		os.Exit(1)
	}
	cancel()
	// DR Boilerplate?

	res := m.Run()
	conn.Close()
	ctx, cancel = context.WithTimeout(context.Background(), time.Minute)
	dsql.DropDB(ctx, &pgCfg, pgxLogger) //nolint:errcheck // test cleanup, don't care if it fails
	cancel()
	os.Exit(res)
}

func TestFooBopCrud(t *testing.T) {
	t.Parallel()
	if !dsqltd.PostgresFound {
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	fooBop := expected.FooBop{
		FooBopCreatable: expected.FooBopCreatable{
			FooBopUpdatable: expected.FooBopUpdatable{
				Bar:    1234,
				Baz:    []byte{0xde, 0xad},
				BipBup: []string{"hello", "world"},
			},
			Blah: true,
		},
	}
	var err error
	fooBop.ID, err = expected.FooBopCreate(ctx, conn, &fooBop.FooBopCreatable)
	require.NoError(t, err)

	var found *expected.FooBop
	found, err = expected.FooBopRead(ctx, conn, fooBop.ID)
	require.NoError(t, err)
	require.Equal(t, &fooBop, found)

	_, err = expected.FooBopRead(ctx, conn, -1)
	require.ErrorIs(t, err, dsql.NotFound)

	found, err = expected.FooBopReadByBar(ctx, conn, fooBop.Bar)
	require.NoError(t, err)
	require.Equal(t, &fooBop, found)

	_, err = expected.FooBopReadByBar(ctx, conn, -1)
	require.ErrorIs(t, err, dsql.NotFound)

	fooBop.Bar = 9876
	fooBop.Baz = []byte{0xbe, 0xef}
	fooBop.BipBup = []string{"updated"}
	err = expected.FooBopUpdate(ctx, conn, fooBop.ID, &fooBop.FooBopUpdatable)
	require.NoError(t, err)
	found, err = expected.FooBopRead(ctx, conn, fooBop.ID)
	require.NoError(t, err)
	require.Equal(t, &fooBop, found)

	err = expected.FooBopUpdate(ctx, conn, -1, &expected.FooBopUpdatable{})
	require.ErrorIs(t, err, dsql.NotFound)

	fooBop.Bar = 2345
	fooBop.Baz = []byte{0xca, 0xfe}
	err = expected.FooBopPatch(ctx, conn, fooBop.ID, &expected.FooBopPatchable{
		Bar: &fooBop.Bar,
		Baz: &fooBop.Baz,
	})
	require.NoError(t, err)
	found, err = expected.FooBopRead(ctx, conn, fooBop.ID)
	require.NoError(t, err)
	require.Equal(t, &fooBop, found)

	fooBop.BipBup = []string{"patched"}
	err = expected.FooBopPatch(ctx, conn, fooBop.ID, &expected.FooBopPatchable{
		BipBup: &fooBop.BipBup,
	})
	require.NoError(t, err)
	found, err = expected.FooBopRead(ctx, conn, fooBop.ID)
	require.NoError(t, err)
	require.Equal(t, &fooBop, found)

	err = expected.FooBopPatch(ctx, conn, -1, &expected.FooBopPatchable{
		BipBup: &fooBop.BipBup,
	})
	require.ErrorIs(t, err, dsql.NotFound)

	err = expected.FooBopDelete(ctx, conn, fooBop.ID)
	require.NoError(t, err)
	_, err = expected.FooBopRead(ctx, conn, fooBop.ID)
	require.ErrorIs(t, err, dsql.NotFound)
	err = expected.FooBopDelete(ctx, conn, fooBop.ID)
	require.ErrorIs(t, err, dsql.NotFound)
}

func findStruct(structs []*metamodel.StructType, name string) (*metamodel.StructType, error) {
	for _, s := range structs {
		if s.Name == name {
			return s, nil
		}
	}
	return nil, derrors.Errorf("struct %q not found", name)
}
