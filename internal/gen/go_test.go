package gen_test

import (
	"bytes"
	_ "embed"
	"testing"

	"codeberg.org/arichard/dcrud/internal/gen"
	"github.com/stretchr/testify/require"
)

var (
	//go:embed expected/foobopcrud.go
	expectedFooBopCrud string
	//go:embed expected/foobophttp.go
	expectedFooBopHTTP string
	//go:embed expected/foobophttpd.go
	expectedFooBopHttpd string
	//go:embed expected/foobopstruct.go
	expectedFooBopStruct string
	//go:embed expected/auditstruct.go
	expectedAuditStruct string
)

func TestCreateFooBopCrud(t *testing.T) {
	t.Parallel()
	g, err := gen.NewGen()
	require.NoError(t, err)
	var buf []byte
	buf, err = g.CreateCrud(fooBop, &bytes.Buffer{})
	require.NoError(t, err)
	require.Equal(t, expectedFooBopCrud, string(buf))
}

func TestCreateFooBopHTTP(t *testing.T) {
	t.Parallel()
	g, err := gen.NewGen()
	require.NoError(t, err)
	var buf []byte
	buf, err = g.CreateHTTP(fooBop, &bytes.Buffer{})
	require.NoError(t, err)
	require.Equal(t, expectedFooBopHTTP, string(buf))
}

func TestCreateFooBopHttpd(t *testing.T) {
	t.Parallel()
	g, err := gen.NewGen()
	require.NoError(t, err)
	var buf []byte
	buf, err = g.CreateHttpd(fooBop, &bytes.Buffer{})
	require.NoError(t, err)
	require.Equal(t, expectedFooBopHttpd, string(buf))
}

func TestCreateFooBopStruct(t *testing.T) {
	t.Parallel()
	g, err := gen.NewGen()
	require.NoError(t, err)
	var buf []byte
	buf, err = g.CreateStruct(fooBop, &bytes.Buffer{})
	require.NoError(t, err)
	require.Equal(t, expectedFooBopStruct, string(buf))
}

func TestCreateAuditStruct(t *testing.T) {
	t.Parallel()
	g, err := gen.NewGen()
	require.NoError(t, err)
	var buf []byte
	buf, err = g.CreateStruct(audit, &bytes.Buffer{})
	require.NoError(t, err)
	require.Equal(t, expectedAuditStruct, string(buf))
}
