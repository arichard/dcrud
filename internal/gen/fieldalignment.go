package gen

import (
	"sort"

	"codeberg.org/arichard/dcrud/internal/metamodel"
)

// Modified from https://cs.opensource.google/go/x/tools/+/master:go/analysis/passes/fieldalignment/fieldalignment.go

const (
	wordSize = 8
	maxAlign = 8
)

func fieldAlign(sfs []*metamodel.StructField) []*metamodel.StructField {
	numFields := len(sfs)

	type elem struct {
		index   int
		alignof int64
		sizeof  int64
		ptrdata int64
	}

	elems := make([]elem, numFields)
	for i, field := range sfs {
		ft := field.Type
		elems[i] = elem{
			index:   i,
			alignof: alignof(ft),
			sizeof:  sizeof(ft),
			ptrdata: ptrdata(ft),
		}
	}

	sort.Slice(elems, func(i, j int) bool {
		elemi := &elems[i]
		elemj := &elems[j]

		// Place zero sized objects before non-zero sized objects.
		zeroi := elemi.sizeof == 0
		zeroj := elemj.sizeof == 0
		if zeroi != zeroj {
			return zeroi
		}

		// Next, place more tightly aligned objects before less tightly aligned objects.
		if elemi.alignof != elemj.alignof {
			return elemi.alignof > elemj.alignof
		}

		// Place pointerful objects before pointer-free objects.
		noptrsi := elemi.ptrdata == 0
		noptrsj := elemj.ptrdata == 0
		if noptrsi != noptrsj {
			return noptrsj
		}

		if !noptrsi {
			// If both have pointers...

			// ... then place objects with less trailing
			// non-pointer bytes earlier. That is, place
			// the field with the most trailing
			// non-pointer bytes at the end of the
			// pointerful section.
			traili := elemi.sizeof - elemi.ptrdata
			trailj := elemj.sizeof - elemj.ptrdata
			if traili != trailj {
				return traili < trailj
			}
		}

		// Lastly, order by size.
		if elemi.sizeof != elemj.sizeof {
			return elemi.sizeof > elemj.sizeof
		}

		return false
	})

	fields := make([]*metamodel.StructField, numFields)
	for i, e := range elems {
		fields[i] = sfs[e.index]
	}
	return fields
}

// Code below based on go/types.StdSizes.

func alignof(typ metamodel.Type) int64 {
	// For arrays and structs, alignment is defined in terms
	// of alignment of the elements and fields, respectively.
	if t, ok := typ.(*metamodel.EmbedType); ok {
		// spec: "For a variable x of struct type: unsafe.Alignof(x)
		// is the largest of the values unsafe.Alignof(x.f) for each
		// field f of x, but at least 1."
		max := int64(1)
		for _, f := range t.Fields {
			if a := alignof(f.Type); a > max {
				max = a
			}
		}
		return max
	}
	align := sizeof(typ) // may be 0
	// spec: "For a variable x of any type: unsafe.Alignof(x) is at least 1."
	if align < 1 {
		return 1
	}
	if align > maxAlign {
		return maxAlign
	}
	return align
}

//nolint:gochecknoglobals,gomnd // is basically a const, ok to use magic numbers
var basicSizes = map[string]byte{
	"bool":       1,
	"int8":       1,
	"int16":      2,
	"int32":      4,
	"int64":      8,
	"uint8":      1,
	"uint16":     2,
	"uint32":     4,
	"uint64":     8,
	"float32":    4,
	"float64":    8,
	"complex64":  8,
	"complex128": 16,
}

func sizeof(typ metamodel.Type) int64 {
	switch stTyp := typ.(type) {
	case *metamodel.PrimitiveType:
		kind := stTyp.GoType.String()
		if s, ok := basicSizes[kind]; ok {
			return int64(s)
		}
		if kind == "string" {
			return wordSize * 2 //nolint:gomnd // in the original code
		}
	case *metamodel.ArrayType:
		return wordSize * 3 //nolint:gomnd // in the original code
	case *metamodel.EmbedType:
		numFields := len(stTyp.Fields)
		if numFields == 0 {
			return 0
		}

		var count int64
		max := int64(1)
		for idx, f := range stTyp.Fields {
			ft := f.Type
			algn, size := alignof(ft), sizeof(ft)
			if algn > max {
				max = algn
			}
			if idx == numFields-1 && size == 0 && count != 0 {
				size = 1
			}
			count = align(count, algn) + size
		}
		return align(count, max)
	}
	return wordSize // catch-all
}

// align returns the smallest y >= x such that y % a == 0.
func align(x, a int64) int64 {
	y := x + a - 1
	return y - y%a
}

func ptrdata(typ metamodel.Type) int64 {
	switch stTyp := typ.(type) {
	case *metamodel.PrimitiveType:
		if stTyp.GoType.String() == "string" {
			return wordSize
		}
		return 0
	case *metamodel.MapType, *metamodel.StructType, *metamodel.ArrayType:
		return wordSize
	case *metamodel.EmbedType:
		nf := len(stTyp.Fields)
		if nf == 0 {
			return 0
		}

		var count, ptr int64
		for _, f := range stTyp.Fields {
			ft := f.Type
			algn, size := alignof(ft), sizeof(ft)
			fp := ptrdata(ft)
			count = align(count, algn)
			if fp != 0 {
				ptr = count + fp
			}
			count += size
		}
		return ptr
	}

	panic("impossible")
}
