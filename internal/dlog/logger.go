package dlog

type Logger interface {
	Print(v ...any)
	Printf(format string, v ...any)
}
