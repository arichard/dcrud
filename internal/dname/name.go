package dname

import (
	"bytes"
	"strings"
	"unicode"
	"unicode/utf8"
)

func EqualIgnoreStyle(lhs, rhs string) bool {
	for lhs != "" && rhs != "" {
		// Ignore underscores
		if lhs[0] == '_' {
			lhs = lhs[1:]
			continue
		}
		if rhs[0] == '_' {
			rhs = rhs[1:]
			continue
		}

		// Grab fist character from each
		var lhsr rune
		var rhsr rune
		{
			var size int
			lhsr, size = utf8.DecodeRuneInString(lhs)
			lhs = lhs[size:]
			rhsr, size = utf8.DecodeRuneInString(rhs)
			rhs = rhs[size:]
		}

		// Compare case
		if rhsr == lhsr {
			continue
		}

		// Ensure strict ordering
		if rhsr < lhsr {
			rhsr, lhsr = lhsr, rhsr
		}

		// Compare knowing case doesn't match
		r := unicode.SimpleFold(lhsr)
		for r != lhsr && r < rhsr {
			r = unicode.SimpleFold(r)
		}
		if r != rhsr {
			return false
		}
	}
	return lhs == rhs
}

func ToCamelCaseAppend(str string, buf *bytes.Buffer) {
	type ccStateType int
	const (
		initial ccStateType = iota
		oneUpper
		twoUpper
		oneLower
	)
	var ccState ccStateType
	var prev rune
	var prevStr string
outer:
	for str != "" {
		curStr := str
		if str[0] == '_' {
			str = str[1:]
			break
		}
		cur, size := utf8.DecodeRuneInString(str)
		str = str[size:]
		switch ccState {
		case initial:
			if unicode.IsUpper(cur) {
				buf.WriteRune(unicode.ToLower(cur))
				ccState = oneUpper
			} else {
				buf.WriteRune(cur)
				ccState = oneLower
			}
		case oneUpper:
			if unicode.IsUpper(cur) {
				prev = unicode.ToLower(cur)
				prevStr = curStr
				ccState = twoUpper
			} else {
				buf.WriteRune(cur)
				ccState = oneLower
			}
		case twoUpper:
			if unicode.IsUpper(cur) {
				buf.WriteRune(prev)
				prev = unicode.ToLower(cur)
				prevStr = curStr
				// Already in twoUpper
			} else {
				prev = 0
				str = prevStr
				break outer
			}
		case oneLower:
			if unicode.IsUpper(cur) {
				str = curStr
				break outer
			}
			buf.WriteRune(cur)
			// Already in oneLower
		}
	}
	if prev != 0 {
		buf.WriteRune(prev)
	}
	ToPascalCaseAppend(str, buf)
}

func ToCamelCase(str string, buf *bytes.Buffer) string {
	buf.Reset()
	ToCamelCaseAppend(str, buf)
	return buf.String()
}

func ToPascalCaseAppend(str string, buf *bytes.Buffer) {
	upperNext := true
	for str != "" {
		if str[0] == '_' {
			str = str[1:]
			upperNext = true
		}
		if upperNext {
			upperNext = false
			cur, size := utf8.DecodeRuneInString(str)
			str = str[size:]
			buf.WriteRune(unicode.ToUpper(cur))
			continue
		}
		i := strings.IndexRune(str, '_')
		if i < 0 {
			i = len(str)
		}
		buf.WriteString(str[:i])
		str = str[i:]
	}
}

func ToPascalCase(str string, buf *bytes.Buffer) string {
	buf.Reset()
	ToPascalCaseAppend(str, buf)
	return buf.String()
}

func ToSnakeCaseAppend(str string, buf *bytes.Buffer) {
	type ccStateType int
	const (
		initial ccStateType = iota
		oneUpper
		twoUpper
		oneLower
	)
	var ccState ccStateType
	var prev rune
	for str != "" {
		if str[0] == '_' {
			if prev != 0 {
				buf.WriteRune(prev)
				prev = 0
			}
			str = str[1:]
			buf.WriteByte('_')
			ccState = initial
			continue
		}
		cur, size := utf8.DecodeRuneInString(str)
		str = str[size:]
		switch ccState {
		case initial:
			if unicode.IsUpper(cur) {
				buf.WriteRune(unicode.ToLower(cur))
				ccState = oneUpper
			} else {
				buf.WriteRune(cur)
				ccState = oneLower
			}
		case oneUpper:
			if unicode.IsUpper(cur) {
				prev = unicode.ToLower(cur)
				ccState = twoUpper
			} else {
				buf.WriteRune(cur)
				ccState = oneLower
			}
		case twoUpper:
			if unicode.IsUpper(cur) {
				buf.WriteRune(prev)
				prev = unicode.ToLower(cur)
				// Already in twoUpper
			} else {
				buf.WriteByte('_')
				buf.WriteRune(prev)
				buf.WriteRune(cur)
				prev = 0
				ccState = oneLower
			}
		case oneLower:
			if unicode.IsUpper(cur) {
				buf.WriteByte('_')
				buf.WriteRune(unicode.ToLower(cur))
				ccState = oneUpper
			} else {
				buf.WriteRune(cur)
				// Already in oneLower
			}
		}
	}
	if prev != 0 {
		buf.WriteRune(prev)
	}
}

func ToSnakeCase(str string, buf *bytes.Buffer) string {
	buf.Reset()
	ToSnakeCaseAppend(str, buf)
	return buf.String()
}
