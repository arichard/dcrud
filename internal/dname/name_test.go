package dname_test

import (
	"bytes"
	"testing"

	"codeberg.org/arichard/dcrud/internal/dname"
	"github.com/stretchr/testify/require"
)

func Test(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		name        string
		camelCase   string
		pascalCase  string
		snakeCase   string
		pascalFancy bool // fancy formatting of API and the like isn't implemented yet
		snakeFancy  bool
	}{{
		camelCase:  "equalIgnoreStyle",
		pascalCase: "EqualIgnoreStyle",
		snakeCase:  "equal_ignore_style",
		/*
			}, {
				camelCase:  "HTMLURLValue",
				pascalCase: "htmlURLValue",
				snakeCase:  "html_url_value",
		*/
	}, {
		camelCase:   "configDNS",
		pascalCase:  "ConfigDNS",
		pascalFancy: true,
		snakeCase:   "config_dns",
		snakeFancy:  true,
	}, {
		camelCase:   "apiName",
		pascalCase:  "APIName",
		pascalFancy: true,
		snakeCase:   "api_name",
	}, {
		camelCase:  "get",
		pascalCase: "Get",
		snakeCase:  "get",
	}}
	require.True(t, dname.EqualIgnoreStyle("", ""))
	require.Equal(t, "url_bad", dname.ToSnakeCase("URL_bad", &bytes.Buffer{}))
	for i := range testCases {
		testCase := &testCases[i]
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			require.False(t, dname.EqualIgnoreStyle("", testCase.camelCase), `"" %q`, testCase.camelCase)
			require.False(t, dname.EqualIgnoreStyle("", testCase.pascalCase), `"" %q`, testCase.pascalCase)
			require.False(t, dname.EqualIgnoreStyle("", testCase.snakeCase), `"" %q`, testCase.snakeCase)
			require.False(t, dname.EqualIgnoreStyle("garbage", testCase.camelCase), `"garbage" %q`, testCase.camelCase)
			require.False(t, dname.EqualIgnoreStyle("garbage", testCase.pascalCase), `"garbage" %q`, testCase.pascalCase)
			require.False(t, dname.EqualIgnoreStyle("garbage", testCase.snakeCase), `"garbage" %q`, testCase.snakeCase)
			require.False(t, dname.EqualIgnoreStyle(testCase.camelCase, ""), `%q ""`, testCase.camelCase)
			require.False(t, dname.EqualIgnoreStyle(testCase.camelCase, "garbage"), `%q "garbage"`, testCase.camelCase)
			require.True(t, dname.EqualIgnoreStyle(testCase.camelCase, testCase.camelCase), "%q %q", testCase.camelCase, testCase.camelCase)
			require.True(t, dname.EqualIgnoreStyle(testCase.camelCase, testCase.pascalCase), "%q %q", testCase.camelCase, testCase.pascalCase)
			require.True(t, dname.EqualIgnoreStyle(testCase.camelCase, testCase.snakeCase), "%q %q", testCase.camelCase, testCase.snakeCase)
			require.False(t, dname.EqualIgnoreStyle(testCase.pascalCase, ""), `%q ""`, testCase.pascalCase)
			require.False(t, dname.EqualIgnoreStyle(testCase.pascalCase, "garbage"), `%q "garbage"`, testCase.pascalCase)
			require.True(t, dname.EqualIgnoreStyle(testCase.pascalCase, testCase.camelCase), "%q %q", testCase.pascalCase, testCase.camelCase)
			require.True(t, dname.EqualIgnoreStyle(testCase.pascalCase, testCase.pascalCase), "%q %q", testCase.pascalCase, testCase.pascalCase)
			require.True(t, dname.EqualIgnoreStyle(testCase.pascalCase, testCase.snakeCase), "%q %q", testCase.pascalCase, testCase.snakeCase)
			require.False(t, dname.EqualIgnoreStyle(testCase.snakeCase, ""), `%q ""`, testCase.snakeCase)
			require.False(t, dname.EqualIgnoreStyle(testCase.snakeCase, "garbage"), `%q "garbage"`, testCase.snakeCase)
			require.True(t, dname.EqualIgnoreStyle(testCase.snakeCase, testCase.camelCase), "%q %q", testCase.snakeCase, testCase.camelCase)
			require.True(t, dname.EqualIgnoreStyle(testCase.snakeCase, testCase.pascalCase), "%q %q", testCase.snakeCase, testCase.pascalCase)
			require.True(t, dname.EqualIgnoreStyle(testCase.snakeCase, testCase.snakeCase), "%q %q", testCase.snakeCase, testCase.snakeCase)

			var buf bytes.Buffer
			require.Equal(t, testCase.camelCase, dname.ToCamelCase(testCase.camelCase, &buf), testCase.camelCase)
			require.Equal(t, testCase.camelCase, dname.ToCamelCase(testCase.pascalCase, &buf), testCase.pascalCase)
			if !testCase.snakeFancy {
				require.Equal(t, testCase.camelCase, dname.ToCamelCase(testCase.snakeCase, &buf), testCase.snakeCase)
			}
			if !testCase.pascalFancy {
				require.Equal(t, testCase.pascalCase, dname.ToPascalCase(testCase.camelCase, &buf), testCase.camelCase)
				require.Equal(t, testCase.pascalCase, dname.ToPascalCase(testCase.snakeCase, &buf), testCase.snakeCase)
			}
			require.Equal(t, testCase.pascalCase, dname.ToPascalCase(testCase.pascalCase, &buf), testCase.pascalCase)
			require.Equal(t, testCase.snakeCase, dname.ToSnakeCase(testCase.camelCase, &buf), testCase.camelCase)
			require.Equal(t, testCase.snakeCase, dname.ToSnakeCase(testCase.pascalCase, &buf), testCase.pascalCase)
			require.Equal(t, testCase.snakeCase, dname.ToSnakeCase(testCase.snakeCase, &buf), testCase.snakeCase)
		})
	}
}
