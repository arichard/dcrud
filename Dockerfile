# syntax=docker/dockerfile:1

FROM golang:1.19 AS build

WORKDIR /dcrud

# Cache downloaded deps in their own layer
COPY go.mod ./
RUN go mod download -x

COPY . ./
RUN ./build.sh && \
    rm -rf ~/.cache && \
    go clean -x -cache -testcache -modcache

FROM gcr.io/distroless/base

LABEL org.opencontainers.image.licenses=BSD-3-Clause
LABEL org.opencontainers.image.source=https://codeberg.org/arichard/dcrud

WORKDIR /

COPY --from=build /dcrud/bin/dcrud /usr/bin/

EXPOSE 8080

USER nonroot:nonroot

ENTRYPOINT ["/usr/bin/dcrud"]
